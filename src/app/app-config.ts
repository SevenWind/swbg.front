import {Injectable} from '@angular/core';
import * as config from './app-config.json';

@Injectable()
export class Config {
  config = {
    MAIL_SERVICE_URL: '',
    SHOP_SERVICE_URL: '',
    USER_SERVICE_URL: '',
    LOOTBOX_SERVICE_URL: '',
    ADMIN_SERVICE_URL: ''
  };

  constructor() {
    this.config = config.config;
  }

}
