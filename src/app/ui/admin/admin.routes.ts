import {Routes} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {AdminLootboxeComponent} from './lootboxes/lootboxes.component';
import { MailComponent } from '../user/mail/mail.component';
import { CalendarConfigComponent } from './calendar-config/calendar-config.component';
import { AdminShopComponent } from './admin-shop/admin-shop.component';

export const ADMIN_ROUTES: Routes = [
  { path: 'admin', component: UsersComponent },
  { path: 'admin/users', component: UsersComponent },
  { path: 'admin/mail', component: MailComponent },
  { path: 'admin/calendar', component: CalendarConfigComponent },
  { path: 'admin/shop', component: AdminShopComponent },
  { path: 'admin/lootboxes', component: AdminLootboxeComponent },
];
