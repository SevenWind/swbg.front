import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Role} from '../../models/enums';
import {AuthService} from '../../services/auth/auth.service';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(protected router: Router, private auth: AuthService) {
  }

  canActivate() {
    if (localStorage.getItem('access_token')) {
      if (this.auth.UserInRole(Role.ADMIN)) {
        return true;
      }
    }
    this.router.navigate(['signin']);
    return false;
  }
}
