import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ItemCostModel } from 'src/app/models/item-cost.model';
import { PaginButtonModel } from 'src/app/models/pagin-button.model';
import { ShopItemModel } from 'src/app/models/shop-item.model';
import { ShopModel } from 'src/app/models/shop.model';
import { ShopService } from 'src/app/services/shop/shop.service';
import { HtmlUtility } from '../../html-utility';
import { MailService } from 'src/app/services/mail/mail.service';
import { ShopFilterModel } from 'src/app/models/shop-filter.model';
import { CalendarService } from 'src/app/services/calendar/calendar.service';
import { PlayerService } from '../../../services/player/player.service';
import { LootboxesComponent } from '../../user/lootboxes/lootboxes.component';
import { LootboxService } from '../../../services/lootbox/lootbox.service';
import { Rarity } from 'src/app/models/enums';

@Component({
  selector: 'app-admin-shop',
  templateUrl: './admin-shop.component.html',
  styleUrls: ['./admin-shop.component.scss']
})
export class AdminShopComponent implements OnInit {

  shop: ShopModel = new ShopModel(
    [],
    0,
    0,
    10
  );
  shopFilter: ShopFilterModel = new ShopFilterModel();

  numberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.shop, 1);
  selectedItem: ShopItemModel = new ShopItemModel('', '', '', 1, '', 0,
    new ItemCostModel(0, 0), new ItemCostModel(0, 0), false, 0);
  currItemImageName: string = '';
  newItem: ShopItemModel = new ShopItemModel('', '', '', 1, '', 1,
    new ItemCostModel(0, 0), new ItemCostModel(0, 0), false, -1);

  constructor(private shopService: ShopService, private toastr: ToastrService,
    private mailService: MailService, private adminService: CalendarService,
    private userService: PlayerService, private lootboxService: LootboxService) {
  }

  ngOnInit() {
    this.fillShop(1);
  }

  prev() {
    if (this.shop.pageNumber - 1 < 1) {
      return;
    }
    this.fillShop(this.shop.pageNumber - 1);
  }

  next() {
    if (this.shop.pageNumber + 1 > this.shop.pageCount) {
      return;
    }
    this.fillShop(this.shop.pageNumber + 1);
  }

  goToPage(pageNumber) {
    this.fillShop(pageNumber);
  }

  private fillShop(pageNum: number) {
    this.shopService.getAllShopItems(pageNum, 24, this.shopFilter).subscribe((data) => {
      this.shop = data;
      this.numberButtons = HtmlUtility.getPaginButtonNumbers(this.shop, data.pageCount);
    });
  }

  sort(fieldName: string, sortDir: number) {
    if (sortDir === 1 || sortDir === -1) {
      sortDir = sortDir * -1;
    }
    if (sortDir === 0) {
      sortDir = 1;
    }
    if (fieldName === 'name') {
      this.shopFilter.nameSort = sortDir;
      this.shopFilter.buyGoldCost = 0;
      this.shopFilter.buyCoinsCost = 0;
      this.shopFilter.sellGoldCost = 0;
      this.shopFilter.sellCoinsCost = 0;
    }
    if (fieldName === 'buyGoldCost') {
      this.shopFilter.nameSort = 0;
      this.shopFilter.buyGoldCost = sortDir;
      this.shopFilter.buyCoinsCost = 0;
      this.shopFilter.sellGoldCost = 0;
      this.shopFilter.sellCoinsCost = 0;
    }
    if (fieldName === 'buyCoinsCost') {
      this.shopFilter.nameSort = 0;
      this.shopFilter.buyGoldCost = 0;
      this.shopFilter.buyCoinsCost = sortDir;
      this.shopFilter.sellGoldCost = 0;
      this.shopFilter.sellCoinsCost = 0;
    }
    if (fieldName === 'sellGoldCost') {
      this.shopFilter.nameSort = 0;
      this.shopFilter.buyGoldCost = 0;
      this.shopFilter.buyCoinsCost = 0;
      this.shopFilter.sellGoldCost = sortDir;
      this.shopFilter.sellCoinsCost = 0;
    }
    if (fieldName === 'sellCoinsCost') {
      this.shopFilter.nameSort = 0;
      this.shopFilter.buyGoldCost = 0;
      this.shopFilter.buyCoinsCost = 0;
      this.shopFilter.sellGoldCost = 0;
      this.shopFilter.sellCoinsCost = sortDir;
    }
    this.fillShop(1);
  }

  toggleLockItem(index: number, islock: boolean) {
    this.shopService.changeLockItem(this.shop.items[index].item.id, islock).subscribe(res => {
      if (res) {
        this.shop.items[index].isAvail = islock;
        this.toastr.success('', 'Предмет ' +
          this.shop.items[index].item.name + ' теперь ' +
          (islock ? '' : 'не') + ' отображается в магазине.');
      }
    });
  }

  onFileChange(event, item: ShopItemModel) {
    if (event.target.files && event.target.files.length) {
      this.currItemImageName = event.target.files[0].name;

      const reader: FileReader = new FileReader();
      reader.onloadend = (e) => {
        this.toastr.success('', 'Изображние загружено');
        item.item.image = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  selectItem(item: ShopItemModel) {
    item.item.rarity = Rarity[item.item.rarity.toString().toUpperCase()];
    this.selectedItem = item;
  }

  clearFilter() {
    this.shopFilter.name = '';
    this.shopFilter.isAvail = 0;
    this.shopFilter.nameSort = 0;
    this.shopFilter.buyGoldCost = 0;
    this.shopFilter.buyCoinsCost = 0;

    this.shopFilter.sellGoldCost = 0;
    this.shopFilter.sellCoinsCost = 0;
    this.fillShop(1);
  }

  deleteItem(itemId: string) {
    // Удаление из магазина
    this.shopService.deleteItem(itemId).subscribe(shopDelResult => {
      if (shopDelResult) {
        this.toastr.success('', 'Предмет успешно удален из SWShopService');
        this.fillShop(1);
      } else {
        this.restoreItem(itemId);
      }
    });

    // Удаление из сервиса лутбоксы
    this.lootboxService.deleteItem(itemId).subscribe(lbDelResult => {
      if (lbDelResult) {
        this.toastr.success('', 'Предмет успешно удален из SWLootboxService');
      } else {
        this.restoreItem(itemId);
      }
    });

    // Удаление из сервиса пользователей
    this.userService.deleteItem(itemId).subscribe(userDelResult => {
      if (userDelResult) {
        this.toastr.success('', 'Предмет успешно удален из SWUserService');
      } else {
        this.restoreItem(itemId);
      }
    });

    // Удаление из swadmin
    this.adminService.deleteItem(itemId).subscribe(adminDelResult => {
      if (adminDelResult) {
        this.toastr.success('', 'Предмет успешно удален из SWAdminService');
      } else {
        this.restoreItem(itemId);
      }
    });

    // Удаление из swmail
    this.mailService.deleteItem(itemId).subscribe(mailDelResult => {
      if (mailDelResult) {
        this.toastr.success('', 'Предмет успешно удален из SWMailService');
      } else {
        this.restoreItem(itemId);
      }
    });
  }

  restoreItem(itemId: string) {
    //Восстановление предмета в сервисах, если произошла ошибка
    this.lootboxService.restoreItem(itemId).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Предмет успешно восстановлен в бд SWLootboxService после неудачного удаления');
      }
    });
    this.userService.restoreItem(itemId).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Предмет успешно восстановлен в бд SWUserService после неудачного удаления');
      }
    });
    this.shopService.restoreItem(itemId).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Предмет успешно восстановлен в бд SWShopService после неудачного удаления');
        this.fillShop(1);
      }
    });
    this.mailService.restoreItem(itemId).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Предмет успешно восстановлен в бд SWMailService после неудачного удаления');
      }
    });
    this.adminService.restoreItem(itemId).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Предмет успешно восстановлен в бд SWAdminService после неудачного удаления');
      }
    });
  }

  saveItem(item: ShopItemModel) {
    // сохранение предметов во всех сервисах
    this.shopService.saveItem(item).subscribe(savedId => {
      if (savedId && savedId !== '00000000-0000-0000-0000-000000000000') {
        this.toastr.success('', 'Предмет "' + item.item.name + '" успешно сохранен в SWShopService!');
        this.fillShop(1);
        item.item.id = savedId;
        this.lootboxService.saveItem(item.item).subscribe(lbSaveResult => {
          if (lbSaveResult) {
            this.toastr.success('', 'Предмет "' + item.item.name + '" успешно сохранен в SWLootBoxService!');
          }
        });
        this.userService.saveItem(item.item).subscribe(lbSaveResult => {
          if (lbSaveResult) {
            this.toastr.success('', 'Предмет "' + item.item.name + '" успешно сохранен в SWUserService!');
          }
        });
        this.adminService.saveItem(item.item).subscribe(lbSaveResult => {
          if (lbSaveResult) {
            this.toastr.success('', 'Предмет "' + item.item.name + '" успешно сохранен в SWAdminService!');
          }
        });
        this.mailService.saveItem(item.item).subscribe(lbSaveResult => {
          if (lbSaveResult) {
            this.toastr.success('', 'Предмет "' + item.item.name + '" успешно сохранен в SWMailService!');
          }
        });
      }
    });
  }
}
