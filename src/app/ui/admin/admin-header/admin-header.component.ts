import { Component, OnInit } from '@angular/core';
import {UserShortInfoModel} from '../../../models/userShortInfo.model';
import {MessageBoxType} from '../../../models/enums';
import {PlayerService} from '../../../services/player/player.service';
import {NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/internal/operators';
import {ReplaySubject} from 'rxjs';
import {MailService} from '../../../services/mail/mail.service';
import {AuthService} from '../../../services/auth/auth.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit {

  logo = '𝓑𝓜𝓞'; // '乃爪ㄖ';//'ℬℳØ';
  unread_message_count = 9;
  active_page_url = this.router.url;

  userInfo: UserShortInfoModel = new UserShortInfoModel('', '../../../assets/player_avatars/anonim-avatar.png', 1, 0, 0, 0, 1000);
  private _routerEvents = new ReplaySubject<NavigationEnd>();

  constructor(private playerService: PlayerService, private router: Router, private mailService: MailService,
              private auth: AuthService ) {
    this.router.events.subscribe(this._routerEvents);
  }

  ngOnInit() {
    this.playerService.getPlayerShortInfo(this.auth.getUserId()).subscribe((data) => {
      this.userInfo = data;
    });

    this._routerEvents
      .pipe(filter(e => e instanceof NavigationEnd))
      .subscribe((e) => {
        this.active_page_url = e.urlAfterRedirects.slice(1);
      });

    this.mailService.getMessages(this.auth.getUserId(), MessageBoxType.Incoming, 1).subscribe(_ => {
      this.unread_message_count = this.mailService.unreadedMessagesCount;
    });
  }

  exitFromApp() {
    this.auth.logout();
  }
}
