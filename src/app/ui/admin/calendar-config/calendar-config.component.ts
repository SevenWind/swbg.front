import { Component, OnInit } from '@angular/core';
import { CalendarRewardModel } from 'src/app/models/calendar-reward.model';
import { ToastrService } from 'ngx-toastr';
import { CalendarService } from 'src/app/services/calendar/calendar.service';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlayerService } from 'src/app/services/player/player.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CalendarModel } from 'src/app/models/calendar.model';
import { ShopModel } from 'src/app/models/shop.model';
import { PaginButtonModel } from 'src/app/models/pagin-button.model';
import { HtmlUtility } from '../../html-utility';
import { ItemModel } from 'src/app/models/item.model';
import { DropModel } from 'src/app/models/drop.model';
import { ShopService } from 'src/app/services/shop/shop.service';

@Component({
  selector: 'app-calendar-config',
  templateUrl: './calendar-config.component.html',
  styleUrls: ['./calendar-config.component.scss']
})
export class CalendarConfigComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal,
    private calendarService: CalendarService, private playerService: PlayerService,
    private toastr: ToastrService, private auth: AuthService,
    private shopService: ShopService) {
    // конфигурация модальных окон
    config.backdrop = 'static';
    config.keyboard = true;
  }

  dailyReward: CalendarRewardModel = new CalendarRewardModel(1, 1, '', '', []);
  d7Reward: CalendarRewardModel = new CalendarRewardModel(1, 1, '', '', []);
  d14Reward: CalendarRewardModel = new CalendarRewardModel(1, 1, '', '', []);
  d21Reward: CalendarRewardModel = new CalendarRewardModel(1, 1, '', '', []);
  dFullReward: CalendarRewardModel = new CalendarRewardModel(1, 1, '', '', []);
  calendar: CalendarModel = new CalendarModel(
    100,
    [],
    []
  );

  selectedDayIndex = 0;
  currentDayNum = 1;

  itemCollection: ShopModel = new ShopModel([], 1, 1, 12);
  itemCollectionNumberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.itemCollection.items, 1);
  selectedItem: DropModel = new DropModel(new ItemModel('', '', 1, ''), 0);
  selectedReward: CalendarRewardModel;

  collection: any;

  ngOnInit() {
    this.currentDayNum = new Date().getDate();
    this.calendarService.getCalendarInfo().subscribe(data => {
      this.calendar = data;
      this.dailyReward = data.rewards.filter(a => a.code === 'DAILY')[0];
      this.d7Reward = data.rewards.filter(a => a.code === '7_DAYS')[0];
      this.d14Reward = data.rewards.filter(a => a.code === '14_DAYS')[0];
      this.d21Reward = data.rewards.filter(a => a.code === '21_DAYS')[0];
      this.dFullReward = data.rewards.filter(a => a.code === 'FULL_MONTH')[0];

      this.playerService.getCalendar(this.auth.getUserId()).subscribe(res => this.calendar.userDayChecks = res);
      this.playerService.getRewardStatuses(this.auth.getUserId()).subscribe(res => this.calendar.userCollectedRewards = res);
    });
  }

  prev() {
    if (this.itemCollection.pageNumber - 1 < 1) {
      return;
    }
    this.fillItems(this.itemCollection.pageNumber - 1);
  }

  next() {
    if (this.itemCollection.pageNumber + 1 > this.itemCollection.pageCount) {
      return;
    }
    this.fillItems(this.itemCollection.pageNumber + 1);
  }

  goToPage(pageNumber) {
    this.fillItems(pageNumber);
  }

  private fillItems(number) {
    this.shopService.getItems(number, 12).subscribe((data) => {
      this.itemCollection = data;
      this.itemCollectionNumberButtons = HtmlUtility.getPaginButtonNumbers(this.itemCollection, data.pageCount);
    });
  }

  selectItemInCollection(context, item: ItemModel) {
    this.selectedItem = new DropModel(item, 0, 0);
    this.modalService.open(context);
  }

  openCalendarDayRewardModal(content, i: number) {
    this.selectedDayIndex = i;
    this.fillItems(1);
    this.collection = this.modalService.open(content);
  }

  openAddRewardItemDialog(context, reward: CalendarRewardModel) {
    this.selectedReward = reward;
    this.fillItems(1);
    this.modalService.open(context);
  }

  changeItemInCalendarCell() {
    this.calendarService.saveCalendarCellReward(this.selectedDayIndex + 1, this.selectedItem).subscribe(result => {
      if (result) {
        this.calendar.days[this.selectedDayIndex] = this.selectedItem;
        this.collection.close();
        this.toastr.success('', 'Награда успешно изменена');
      }
    });
  }

  removeReward(rewardModel: CalendarRewardModel, item: DropModel) {
    this.calendarService.removeReward(rewardModel.id, item).subscribe(result => {
      if (result) {
        rewardModel.items = rewardModel.items.filter(a => a !== item);
        this.toastr.success('', 'Предмет успешно удален');
      }
    });
  }

  addReward() {
    this.calendarService.addReward(this.selectedReward.id, this.selectedItem).subscribe(result => {
      if (result) {
        if (this.selectedReward.items.filter(a => a.item.id === this.selectedItem.item.id).length) {
          this.selectedReward.items.filter(a => a.item.id === this.selectedItem.item.id)[0].count = this.selectedItem.count;
        } else {
          this.selectedReward.items.push(this.selectedItem);
          this.selectedReward.emptyItems.pop();
        }
        this.toastr.success('', 'Предмет успешно добавлен');
      }
    });
  }

  saveCheckOldCellGoldCost() {
    this.calendarService.saveCheckOldCellGoldCost(this.calendar.checkOldCellGoldCost).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Цена сохранена');
      }
    });
  }
}
