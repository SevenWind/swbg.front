import { Component, HostListener, OnInit } from '@angular/core';
import { TreasureModel } from '../../../models/treasure.model';
import { DropInfoModel } from '../../../models/dropInfo.model';
import { ToastrService } from 'ngx-toastr';
import { HtmlUtility } from '../../html-utility';
import { ItemModel } from '../../../models/item.model';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ResourceModel } from '../../../models/resource.model';
import { AdminLootboxService } from '../../../services/lootbox-admin/lootbox-admin.service';
import { LootboxService } from '../../../services/lootbox/lootbox.service';
import { PaginButtonModel } from '../../../models/pagin-button.model';
import { ShopModel } from '../../../models/shop.model';
import { DropModel } from '../../../models/drop.model';
import { ShopService } from '../../../services/shop/shop.service';

@Component({
  selector: 'app-admin-lootboxes',
  templateUrl: './lootboxes.component.html',
  styleUrls: ['./lootboxes.component.scss']
})
export class AdminLootboxeComponent implements OnInit {

  treasures: TreasureModel[] = [];
  selectedTreasure: TreasureModel = new TreasureModel('', '', '', [], '', 0, true);
  newTreasure: TreasureModel = new TreasureModel('00000000-0000-0000-0000-000000000000', '', '', [], '', 0, true);
  selectedTreasureDropInfo: DropInfoModel[] = [];
  selectedDropItem: DropInfoModel = new DropInfoModel(new ItemModel('', '', 1, ''), 1, 1, 1, 1);

  itemCollection: ShopModel = new ShopModel([], 1, 1, 12);
  itemCollectionNumberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.itemCollection.items, 1);
  selectedItem: DropModel = new DropModel(new ItemModel('', '', 1, ''), 0);
  addItemModal: any;
  notDeletedResources: ResourceModel[] = [];
  notDeletedDropItems: DropInfoModel[] = [];

  constructor(config: NgbModalConfig, private modalService: NgbModal,
    public adminLootboxService: AdminLootboxService, private toastr: ToastrService,
    public lootboxService: LootboxService, private shopService: ShopService) {
    // конфигурация модальных окон
    config.backdrop = 'static';
    config.keyboard = true;
  }

  ngOnInit() {
    this.fillTreasures();
  }

  prev() {
    if (this.itemCollection.pageNumber - 1 < 1) {
      return;
    }
    this.fillItems(this.itemCollection.pageNumber - 1);
  }

  next() {
    if (this.itemCollection.pageNumber + 1 > this.itemCollection.pageCount) {
      return;
    }
    this.fillItems(this.itemCollection.pageNumber + 1);
  }

  goToPage(pageNumber) {
    this.fillItems(pageNumber);
  }

  private fillTreasures() {
    this.adminLootboxService.getTreasures().subscribe((data) => {
      this.treasures = data;
    });
  }

  private fillItems(number) {
    this.shopService.getItems(number, 12).subscribe((data) => {
      this.itemCollection = data;
      this.itemCollectionNumberButtons = HtmlUtility.getPaginButtonNumbers(this.itemCollection, data.pageCount);
    });
  }

  openBoxEdit(content, box: TreasureModel) {
    this.selectedTreasure = box;
    this.notDeletedResources = this.selectedTreasure.resources.filter(a => a.isDeleted === false);
    this.lootboxService.getDropInfo(box.id).subscribe(data => {
      this.selectedTreasureDropInfo = data.lootInfo;
      this.notDeletedDropItems = this.selectedTreasureDropInfo.filter(a => a.isDeleted === false);
      this.modalService.open(content);
    });
  }

  openAddLootBox(context) {
    this.fillItems(1);
    this.addItemModal = this.modalService.open(context);
  }

  addItemToDrop() {
    this.selectedTreasureDropInfo.push(new DropInfoModel(this.selectedItem.item, 1, 0, this.selectedItem.count, 0));
    this.notDeletedDropItems = this.selectedTreasureDropInfo.filter(a => a.isDeleted === false);
    this.addItemModal.close();
  }

  removeDropItem(context, drop: DropInfoModel) {
    const confirm = this.modalService.open(context);
    confirm.result.then((userResponse) => {
      if (userResponse) {
        this.selectedTreasureDropInfo[this.selectedTreasureDropInfo.indexOf(drop)].isDeleted = true;
        this.notDeletedDropItems = this.selectedTreasureDropInfo.filter(a => a.isDeleted === false);
      }
    });
  }

  selectBox(box: TreasureModel) {
    this.selectedTreasure = box;
    this.notDeletedResources = this.selectedTreasure.resources.filter(a => a.isDeleted === false);
    this.notDeletedDropItems = this.selectedTreasureDropInfo.filter(a => a.isDeleted === false);
    this.lootboxService.getDropInfo(box.id).subscribe(data => {
      this.selectedTreasureDropInfo = data.lootInfo;
    });
  }

  lockLootBox() {
    this.adminLootboxService.lockTreasure(this.selectedTreasure.id).subscribe(res => {
      if (res) {
        this.toastr.success('', this.selectedTreasure.name + ' заблокирован');
        this.fillTreasures();
      }
    });
  }

  unLockLootBox() {
    this.adminLootboxService.unLockTreasure(this.selectedTreasure.id).subscribe(res => {
      if (res) {
        this.toastr.success('', this.selectedTreasure.name + ' разблокирован');
        this.fillTreasures();
      }
    });
  }

  selectDropItem(drop: DropInfoModel) {
    this.selectedDropItem = drop;
  }

  removeItemResources(item: any) {
    const index = this.selectedTreasure.resources.indexOf(item);
    this.selectedTreasure.resources[index].isDeleted = true;
    this.selectedTreasure.emptyResources.push({});
    this.notDeletedResources = this.selectedTreasure.resources.filter(a => a.isDeleted === false);
  }

  addItemToResources() {
    this.selectedTreasure.resources.push(new ResourceModel(this.selectedItem.item, this.selectedItem.count));
    this.notDeletedResources = this.selectedTreasure.resources.filter(a => a.isDeleted === false);
    this.selectedTreasure.emptyResources.pop();
    this.addItemModal.close();
  }

  selectItemInCollection(context, item: ItemModel) {
    this.selectedItem = new DropModel(item, 0, 0);
    this.modalService.open(context);
  }

  getDisplayCount(value: number) {
    return HtmlUtility.formatLongNumber(value);
  }

  saveTreasure() {
    this.adminLootboxService.saveTreasure(this.selectedTreasure).subscribe(res => {
      if (res) {
        this.adminLootboxService.saveDropInfo(this.selectedTreasure.id, this.selectedTreasureDropInfo).subscribe(dres => {
          if (dres) {
            this.toastr.success('', this.selectedTreasure.name + ' успешно сохранен');
            this.modalService.dismissAll();
          }
        });
      }
    });
  }

  saveNewTreasure() {
    this.newTreasure.id = '00000000-0000-0000-0000-000000000000';
    this.adminLootboxService.saveTreasure(this.newTreasure).subscribe(res => {
      if (res) {
        this.toastr.success('', this.selectedTreasure.name + ' успешно создан');
        this.modalService.dismissAll();
      }
    });
  }

  openNewTreasureForm(context) {
    this.modalService.open(context);
  }

  openAddItemModal(context) {
    this.fillItems(1);
    this.addItemModal = this.modalService.open(context);
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      this.selectedTreasure.imgName = event.target.files[0].name;

      const reader: FileReader = new FileReader();
      reader.onloadend = (e) => {
        this.toastr.success('', 'Изображние загружено');
        this.selectedTreasure.image = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }
}
