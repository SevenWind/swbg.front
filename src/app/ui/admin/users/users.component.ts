import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AdminUsersService} from '../../../services/admin-users/admin-users.service';
import {UsersListModel} from '../../../models/users-list.model';
import {UserListItemModel} from '../../../models/user-list-item.model';
import {HtmlUtility} from '../../html-utility';
import {PaginButtonModel} from '../../../models/pagin-button.model';
import {UserFilterModel} from '../../../models/user-filter.model';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../../services/auth/auth.service';
import {MailModel} from '../../../models/mail.model';
import {DropModel} from '../../../models/drop.model';
import {ItemModel} from '../../../models/item.model';
import {ShopService} from '../../../services/shop/shop.service';
import {ShopModel} from '../../../models/shop.model';
import {MailService} from '../../../services/mail/mail.service';

declare var $: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {

  users: UsersListModel = new UsersListModel([], new UserFilterModel(), 1, 1);
  userListPageSize: number = 10;
  numberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.users.users, 1);

  selectedUser: UserListItemModel = new UserListItemModel('', '', 0, '', new Date(), new Date(), false);
  addGoldValue: number = 0;

  mail: MailModel = new MailModel('', '00000000-0000-0000-0000-000000000000', '',
    '', '', '', new Date(), '', false, false, []);
  mailFromArray: any = [];
  selectedItem: DropModel;

  itemCollection: ShopModel = new ShopModel([], 1, 1, 12);
  itemCollectionNumberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.itemCollection.items, 1);

  constructor(private usersService: AdminUsersService, private toastr: ToastrService,
              private auth: AuthService, private shopService: ShopService,
              private mailService: MailService) {
  }

  ngOnInit() {
    this.filterUsers();
    this.mailFromArray = [
      {fromId: this.auth.getUserId(), fromName: this.auth.getUserName()},
      {fromId: '00000000-0000-0000-0000-000000000000', fromName: 'Администрация'}
    ];
    this.selectedItem = new DropModel(new ItemModel('', '', 0, ''), 0, 0);
  }

  ngAfterViewInit() {
    const $this = this;
    $('.datepicker').datepicker({
      onSelect: function (v) {
        if ($(this).hasClass('fromDate')) {
          $this.users.filter.fromRegDate = v;
        } else {
          $this.users.filter.toRegDate = v;
        }
      }
    });
  }

  prev() {
    if (this.users.pageNumber - 1 < 1) {
      return;
    }
    this.filterUsers(this.users.pageNumber - 1);
  }

  next() {
    if (this.users.pageNumber + 1 > this.users.pageCount) {
      return;
    }
    this.filterUsers(this.users.pageNumber + 1);
  }

  goToPage(pageNumber) {
    this.filterUsers(pageNumber);
  }

  filterUsers(pageNumber: number = 1) {
    this.users.filter.fromLvl = this.users.filter.fromLvl === null ? 0 : this.users.filter.fromLvl;
    this.users.filter.toLvl = this.users.filter.toLvl === null ? 0 : this.users.filter.toLvl;

    this.usersService.getUsers(this.users.filter, pageNumber, this.userListPageSize).subscribe(data => {
      this.users = data;
      this.numberButtons = HtmlUtility.getPaginButtonNumbers(this.users.users, data.pageCount);
    });
  }

  sort(fieldName: string, sortDir: number) {
    if (sortDir === 1 || sortDir === -1) {
      sortDir = sortDir * -1;
    }
    if (sortDir === 0) {
      sortDir = 1;
    }
    if (fieldName === 'name') {
      this.users.filter.nameSort = sortDir;
      this.users.filter.lvlSort = 0;
      this.users.filter.regDateSort = 0;
      this.users.filter.emailSort = 0;
      this.users.filter.lastActivitySort = 0;
    }
    if (fieldName === 'regDate') {
      this.users.filter.regDateSort = sortDir;
      this.users.filter.nameSort = 0;
      this.users.filter.lvlSort = 0;
      this.users.filter.emailSort = 0;
      this.users.filter.lastActivitySort = 0;
    }
    if (fieldName === 'lvl') {
      this.users.filter.lvlSort = sortDir;
      this.users.filter.nameSort = 0;
      this.users.filter.regDateSort = 0;
      this.users.filter.emailSort = 0;
      this.users.filter.lastActivitySort = 0;
    }
    if (fieldName === 'lastActivity') {
      this.users.filter.lastActivitySort = sortDir;
      this.users.filter.nameSort = 0;
      this.users.filter.lvlSort = 0;
      this.users.filter.regDateSort = 0;
      this.users.filter.emailSort = 0;
    }
    if (fieldName === 'email') {
      this.users.filter.emailSort = sortDir;
      this.users.filter.nameSort = 0;
      this.users.filter.lvlSort = 0;
      this.users.filter.regDateSort = 0;
      this.users.filter.lastActivitySort = 0;
    }
    this.filterUsers();
  }

  resetFilter() {
    this.users.filter = new UserFilterModel();
    this.filterUsers();
  }

  selectUser(user: UserListItemModel) {
    this.selectedUser = user;
  }

  setAllChecks(value: any) {
    this.users.users.map(a => a.isChecked = value);
  }

  lockUser() {
    this.usersService.setUsersLockStatus([this.selectedUser.id], true).subscribe(res => {
        if (res) {
          this.toastr.success('', 'Пользователь ' + this.selectedUser.name + ' заблокирован');
          this.filterUsers();
        }
      }
    );
  }

  lockUsers() {
    this.usersService.setUsersLockStatus((this.users.users.filter(a => a.isChecked === true)).map(a => a.id), true).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Пользователи заблокированы');
        this.filterUsers();
      }
    });
  }

  unLockUser() {
    this.usersService.setUsersLockStatus([this.selectedUser.id], false).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Пользователь ' + this.selectedUser.name + ' разблокирован');
        this.filterUsers();
      }
    });
  }

  unLockUsers() {
    this.usersService.setUsersLockStatus(this.users.users.filter(a => a.isChecked === true).map(a => a.id), false).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Пользователи разблокированы');
        this.filterUsers();
      }
    });
  }

  addGoldToUser() {
    this.usersService.addGoldToUsers([this.selectedUser.id], this.addGoldValue).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Золото начислено пользователю ' + this.selectedUser.name);
        this.filterUsers();
      }
    });
  }

  addGoldToUsers() {
    this.usersService.addGoldToUsers(this.users.users.filter(a => a.isChecked === true).map(a => a.id),
      this.addGoldValue).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Золото начислено пользователям');
        this.filterUsers();
      }
    });
  }

  getCheckedUsers() {
    return this.users.users.filter(a => a.isChecked === true);
  }

  selectItemInCollection(item: ItemModel) {
    this.selectedItem = new DropModel(item, 0, 0);
  }

  getItemList() {
    this.shopService.getAllShopItems(1, 12).subscribe((data) => {
      this.itemCollection = data;
      this.itemCollectionNumberButtons = HtmlUtility.getPaginButtonNumbers(this.itemCollection, data.pageCount);
    });
  }

  sendMessages() {
    this.mail.fromName = this.mailFromArray.filter(a => a.fromId === this.mail.fromId)[0].fromName;
    this.mailService.sendMessages(this.mail, this.users.users.filter(a => a.isChecked === true).map(a => a.id)).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Сообщения успешно доставлены');
      }
    });
  }

  sendMessage() {
    this.mail.fromName = this.mailFromArray.filter(a => a.fromId === this.mail.fromId)[0].fromName;
    this.mail.id = '00000000-0000-0000-0000-000000000000';
    this.mail.toId = this.selectedUser.id;
    this.mail.toName = this.selectedUser.name;
    this.mailService.sendMessageWithAttach(this.mail).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Сообщение успешно доставлено');
      }
    });
  }

  addItemToMailAttach() {
    this.selectedItem.displayCount = HtmlUtility.formatLongNumber(this.selectedItem.count);
    this.mail.items.push(this.selectedItem);
    this.mail.emptyItems.pop();
    $('#addItem').modal('hide');
  }

  removeItemFromMailAttach(item: DropModel) {
    this.mail.items = this.mail.items.filter(a => a.item !== item.item);
    this.mail.emptyItems.push({});
  }
}

