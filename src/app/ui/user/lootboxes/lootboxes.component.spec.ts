import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LootboxesComponent } from './lootboxes.component';

describe('LootboxesComponent', () => {
  let component: LootboxesComponent;
  let fixture: ComponentFixture<LootboxesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LootboxesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LootboxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
