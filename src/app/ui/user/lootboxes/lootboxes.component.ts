import {Component, OnInit} from '@angular/core';
import {TreasureModel} from 'src/app/models/treasure.model';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {DropModel} from '../../../models/drop.model';
import {LootboxService} from '../../../services/lootbox/lootbox.service';
import {PlayerService} from '../../../services/player/player.service';
import {ResourceModel} from '../../../models/resource.model';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../../services/auth/auth.service';

@Component({
  selector: 'app-lootboxes',
  templateUrl: './lootboxes.component.html',
  styleUrls: ['./lootboxes.component.scss']
})
export class LootboxesComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal,
              public lootboxService: LootboxService, private auth: AuthService,
              private playerService: PlayerService, private toastr: ToastrService) {
    // конфигурация модальных окон
    config.backdrop = 'static';
    config.keyboard = true;
  }

  treasures: TreasureModel[] = [];

  treasureInfo: any = {};

  treasureDrop: DropModel[] = [];

  ngOnInit() {
    this.lootboxService.getTreasures().subscribe((data) => {
      this.treasures = data;
    });
  }

  public openLoot(content, treasureId, count = 1) {
    const treasure = this.treasures.filter(a => a.id === treasureId)[0];
    if (!treasure) {
      return;
    }
    this.playerService.canOpenTreasure(this.auth.getUserId(), count, treasure.resources).subscribe(
      res => {
        // Если списание ресурсов прошло неудачно не идем дальше
        if (!res) {
          return;
        }
        this.lootboxService.open(this.auth.getUserId(), treasureId, count, treasure.resources).subscribe((data) => {
          // Если при открытии лутбокса выпала ошибка - вернется callback с возвратом ресурсов пользователю
          if (!data) {
            this.returnItems(this.auth.getUserId(), count, treasure.resources);
            return;
          }
          this.playerService.addItemsToInventory(this.auth.getUserId(), data, true).subscribe(addRes => {
            if (!addRes) {
              this.returnItems(this.auth.getUserId(), count, treasure.resources);
              return;
            }
            this.playerService.addExp(this.auth.getUserId(), treasure.exp).subscribe();
            this.toastr.success('', 'Выпавшие предметы успешно добвалены в инвентарь');
            this.playerService.updateCash(this.auth.getUserId()).subscribe();
            this.treasureDrop = data;
            this.modalService.open(content);
          });
        });
      }
    );
  }

  public openInfo(content, treasureId) {
    this.lootboxService.getDropInfo(treasureId).subscribe((data: {}) => {
      this.treasureInfo = data;
      this.modalService.open(content);
    });
  }

  private returnItems(playerId: string, count: number, resources: ResourceModel[]) {
    this.playerService.returnTreasureResources(playerId, count, resources)
      .subscribe(res => this.toastr.success('', 'Ресурсы были возвращены.'));
  }
}

