import { Component, OnInit } from '@angular/core';
import { MailModel } from '../../../models/mail.model';
import { MailService } from '../../../services/mail/mail.service';
import { MessageBoxType } from '../../../models/enums';
import { MailBoxModel } from '../../../models/mailbox.model';
import { PaginButtonModel } from '../../../models/pagin-button.model';
import { HtmlUtility } from '../../html-utility';
import { PlayerService } from '../../../services/player/player.service';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../../services/auth/auth.service';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DropModel } from 'src/app/models/drop.model';
import { ShopModel } from 'src/app/models/shop.model';
import { ItemModel } from 'src/app/models/item.model';
import { ShopService } from 'src/app/services/shop/shop.service';

declare var $: any;

@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.scss']
})
export class MailComponent implements OnInit {
  currentIncomingMail: MailModel;
  currentOutgoingMail: MailModel;
  incomingMails: MailBoxModel = new MailBoxModel([], 1, 1);
  outgoingMails: MailBoxModel = new MailBoxModel([], 1, 1);

  searchString = '';
  isAdmin = false;

  incomingNumberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.incomingMails, 1);
  outgoingNumberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.outgoingMails, 1);

  newMessage: MailModel = new MailModel('', '', '', '', '', '', new Date(), '', false, false, []);
  newMsgAttach: DropModel[] = [];
  newMsgEmptyAttach: any[] = Array(12).fill('');

  itemCollection: ShopModel = new ShopModel([], 1, 1, 12);
  itemCollectionNumberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.itemCollection.items, 1);
  selectedItem: DropModel = new DropModel(new ItemModel('', '', 1, ''), 0);
  addItemModal: any;

  constructor(config: NgbModalConfig, private modalService: NgbModal,
    private mailService: MailService, private playerService: PlayerService,
    private toastr: ToastrService, private shopService: ShopService,
    private auth: AuthService) {
    // конфигурация модальных окон
    config.backdrop = 'static';
    config.keyboard = true;
  }

  ngOnInit() {
    this.fillMails(MessageBoxType.Incoming, 1);
    this.fillMails(MessageBoxType.Outgoing, 1);
    this.isAdmin = this.auth.UserInRole('Admin');

    const $this = this;
    $('#query').typeahead({
      source: function (query, process) {
        return $this.playerService.getMailAddresses(query).subscribe(
          data => {
            return process(data);
          });
      },
      updater: function (item) {
        $this.newMessage.toId = item.id;
        $this.newMessage.toName = item.name;
        return item;
      }
    });
  }

  prev(type: MessageBoxType) {
    const mails = this.getMails(type);
    if (mails.pageNumber - 1 < 1) {
      return;
    }
    this.fillMails(type, mails.pageNumber - 1);
  }

  next(type: MessageBoxType) {
    const mails = this.getMails(type);
    if (mails.pageNumber + 1 > mails.pageCount) {
      return;
    }
    this.fillMails(type, mails.pageNumber + 1);
  }

  goToPage(type: MessageBoxType, pageNumber) {
    this.fillMails(type, pageNumber);
  }

  searchMails(type: MessageBoxType) {
    this.fillMails(type, 1, this.searchString);
  }

  private fillMails(type: MessageBoxType, pageNum: number, search: string = '_') {
    if (search === '') {
      search = '_';
    }
    this.mailService.getMessages(this.auth.getUserId(), type, pageNum, 10, search).subscribe(data => {
      if (!data) {
        return;
      }
      if (type === MessageBoxType.Incoming) {
        this.incomingMails = data;
        this.incomingNumberButtons = HtmlUtility.getPaginButtonNumbers(this.incomingMails, data.pageCount);
      } else {
        this.outgoingMails = data;
        this.outgoingNumberButtons = HtmlUtility.getPaginButtonNumbers(this.outgoingMails, data.pageCount);
      }
    });
  }

  private getMails(type: MessageBoxType): MailBoxModel {
    if (type === MessageBoxType.Incoming) {
      return this.incomingMails;
    } else {
      return this.outgoingMails;
    }
  }

  delMessage(type: MessageBoxType, mailId: string) {
    this.mailService.deleteMesage(this.auth.getUserId(), mailId).subscribe(_ => {
      this.toastr.success('', 'Сообщение успешно удалено');
      this.fillMails(type, 1);
    });
  }

  collectAttachment(mailId: string) {
    const mail = this.incomingMails.mails.filter(a => a.id === mailId)[0];
    if (!mail) {
      return;
    }
    if (mail.attachmentCollected) {
      return;
    }
    this.mailService.collectAttachment(this.auth.getUserId(), mail).subscribe(_ => {
      this.toastr.success('', 'Предметы успешно добавлены в инвентарь');
      this.currentIncomingMail.attachmentCollected = true;
      this.fillMails(MessageBoxType.Incoming, 1);
    });
  }

  openMail(type: MessageBoxType, index: number) {
    if (type === MessageBoxType.Incoming) {
      this.currentIncomingMail = this.incomingMails.mails[index];
    } else {
      this.currentOutgoingMail = this.outgoingMails.mails[index];
    }
  }

  toggleCheckMail(type: MessageBoxType, index: number) {
    if (type === MessageBoxType.Incoming) {
      this.incomingMails.mails[index].isChecked = !this.incomingMails.mails[index].isChecked;
    } else {
      this.outgoingMails.mails[index].isChecked = !this.outgoingMails.mails[index].isChecked;
    }
  }

  delManyMails(type: MessageBoxType) {
    let mails = [];
    if (type === MessageBoxType.Incoming) {
      mails = this.incomingMails.mails.filter(a => a.isChecked);
    } else {
      mails = this.outgoingMails.mails.filter(a => a.isChecked);
    }
    this.mailService.deleteMesages(this.auth.getUserId(), mails.map(mail => mail.id)).subscribe(_ => {
      this.toastr.success('', 'Сообщения успешно удалены');
      this.fillMails(type, 1);
    });
  }

  checkMail(type: MessageBoxType, index: number) {
    let mailId;
    if (type === MessageBoxType.Incoming) {
      mailId = this.incomingMails.mails[index].id;
    } else {
      mailId = this.outgoingMails.mails[index].id;
    }
    this.mailService.checkMesage(this.auth.getUserId(), mailId).subscribe(_ => {
      this.fillMails(type, 1);
    });
  }

  setNewMailTo(toId: string, toName: string) {
    this.newMessage.toName = toName;
    this.newMessage.toId = toId;
    $('#query').val(toName);
  }

  sendNewMessage() {
    if (!this.newMessage.toId) {
      return;
    }
    this.newMessage.fromId = this.auth.getUserId();
    this.newMessage.fromName = this.auth.getUserName();
    this.newMessage.id = '00000000-0000-0000-0000-000000000000';
    this.newMessage.items = this.newMsgAttach;
    this.mailService.sendMessage(this.newMessage).subscribe(_ => {
      this.toastr.success('', 'Сообщение успешно отправлено');
      this.modalService.dismissAll();
      this.fillMails(MessageBoxType.Outgoing, 1);
    });
  }

  showNewMsgForm(context) {
    this.modalService.open(context);
    const $this = this;
    $('#query').typeahead({
      source: function (query, process) {
        return $this.playerService.getMailAddresses(query).subscribe(
          data => {
            return process(data);
          });
      },
      updater: function (item) {
        $this.newMessage.toId = item.id;
        $this.newMessage.toName = item.name;
        return item;
      }
    });
  }

  selectItemInCollection(item: DropModel) {
    this.selectedItem = item;
  }

  addItemToAttach() {
    this.newMsgAttach.push(new DropModel(this.selectedItem.item, this.selectedItem.count, 0));
    this.newMsgEmptyAttach.pop();
    $('#addItem').modal('hide');
  }

  removeItemFromAttach(item: DropModel) {
    this.newMsgEmptyAttach.push({});
    this.newMsgAttach = this.newMsgAttach.filter(a => a !== item);
  }

  openAddItem(context) {
    this.fillItems(1);
    this.openForm(context);
  }

  openForm(context) {
    this.modalService.open(context);
  }

  prevItemPage() {
    if (this.itemCollection.pageNumber - 1 < 1) {
      return;
    }
    this.fillItems(this.itemCollection.pageNumber - 1);
  }

  nextItemPage() {
    if (this.itemCollection.pageNumber + 1 > this.itemCollection.pageCount) {
      return;
    }
    this.fillItems(this.itemCollection.pageNumber + 1);
  }

  goToItemPage(pageNumber) {
    this.fillItems(pageNumber);
  }

  private fillItems(number) {
    this.shopService.getItems(number, 12).subscribe((data) => {
      this.itemCollection = data;
      this.itemCollectionNumberButtons = HtmlUtility.getPaginButtonNumbers(this.itemCollection, data.pageCount);
    });
  }
}
