import { Component, OnInit } from '@angular/core';
import {PlayerService} from '../../../services/player/player.service';
import {RatingItemModel} from '../../../models/rating-item.model';
import {PaginButtonModel} from '../../../models/pagin-button.model';
import {RatingModel} from '../../../models/rating.model';
import {HtmlUtility} from '../../html-utility';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  constructor(public playerService: PlayerService) { }

  rating: RatingModel = new RatingModel([
    new RatingItemModel(1, 'ХубаБуба', 13, 13657, 17510),
    new RatingItemModel(2, 'Гуру', 12, 13, 15780),
    new RatingItemModel(3, 'Ололошка', 1, 400, 1000),
  ], 1, 1);

  numberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.rating, 1);

  ngOnInit() {
    this.fillRating(1);
  }

  prev() {
    if (this.rating.pageNumber - 1 < 1) {
      return;
    }
    this.fillRating(this.rating.pageNumber - 1);
  }

  next() {
    if (this.rating.pageNumber + 1 > this.rating.pageCount) {
      return;
    }
    this.fillRating(this.rating.pageNumber + 1);
  }

  goToPage(pageNumber) {
    this.fillRating(pageNumber);
  }

  private fillRating(pageNum: number) {
    this.playerService.getRating(pageNum).subscribe((data) => {
      this.rating = data;
      this.numberButtons = HtmlUtility.getPaginButtonNumbers(this.rating, data.pageCount);
    });
  }
}
