import {Component, OnInit} from '@angular/core';
import {ShopModel} from '../../../../models/shop.model';
import {ShopCurrency} from '../../../../models/enums';
import {ShopItemModel} from '../../../../models/shop-item.model';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {PlayerService} from '../../../../services/player/player.service';
import {HtmlUtility} from '../../../html-utility';
import {PaginButtonModel} from '../../../../models/pagin-button.model';
import {ShopService} from '../../../../services/shop/shop.service';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../../../services/auth/auth.service';

@Component({
  selector: 'app-profile-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {

  searchItemName = '';
  selectedCurrency = ShopCurrency['COINS'];
  selectedItemCost = 0;
  selectedItem: ShopItemModel;
  currSelectedItemCount = 1;
  maxSelectedItemCount = 1;
  inventory: ShopModel = new ShopModel(
    [],
    0,
    0,
  );
  numberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.inventory, 1);

  constructor(config: NgbModalConfig, private modalService: NgbModal,
              private playerService: PlayerService, private shopService: ShopService,
              private toastr: ToastrService, private auth: AuthService) {
    // конфигурация модальных окон
    config.backdrop = 'static';
    config.keyboard = true;
  }

  ngOnInit() {
    this.fillInventory(1);
  }

  prev() {
    if (this.inventory.pageNumber - 1 < 1) {
      return;
    }
    this.fillInventory(this.inventory.pageNumber - 1);
  }

  next() {
    if (this.inventory.pageNumber + 1 > this.inventory.pageCount) {
      return;
    }
    this.fillInventory(this.inventory.pageNumber + 1);
  }

  goToPage(pageNumber) {
    this.fillInventory(pageNumber);
  }

  private fillInventory(pageNum: number) {
    this.playerService.getInventoryItems(this.auth.getUserId(), '', pageNum, 24).subscribe((data) => {
      this.inventory = data;
      this.numberButtons = HtmlUtility.getPaginButtonNumbers(this.inventory, data.pageCount);
    });
  }

  selectItem(itemId: string) {
    this.selectedItem = this.inventory.items.filter(item => item.item.id === itemId)[0];
  }

  sellItem(itemId: string) {
    this.modalService.dismissAll();
    const currencyType = Object.keys(ShopCurrency).filter(k => ShopCurrency[k as any] === this.selectedCurrency)[0];
    let cost = 0;
    if (this.selectedCurrency === ShopCurrency['GOLD']) {
      cost = this.selectedItem.sellCost.gold;
    } else {
      cost = this.selectedItem.sellCost.coins;
    }
    this.playerService.sellItem(this.auth.getUserId(), itemId, this.currSelectedItemCount, currencyType, cost).subscribe(
      res => {
        this.toastr.success('', 'Предмет успешно продан!');
        this.fillInventory(1);
      }
    );
  }

  removeItem(itemId: string) {
    this.modalService.dismissAll();
    this.playerService.sellItem(this.auth.getUserId(), itemId, this.selectedItem.count, 'COINS', 0).subscribe(
      res => {
        this.toastr.success('', 'Предмет успешно удален!');
        this.fillInventory(1);
      }
    );
  }

  filterItems(pageNum: number) {
    this.playerService.getInventoryItems(this.auth.getUserId(), this.searchItemName, pageNum, 24).subscribe((data) => {
      this.inventory = data;
      this.numberButtons = HtmlUtility.getPaginButtonNumbers(this.inventory, data.pageCount);
    });
  }

  setCurrency(currency: string) {
    if (currency === 'gold') {
      this.selectedCurrency = ShopCurrency['GOLD'];
      this.selectedItemCost = this.selectedItem.sellCost.gold;
    } else {
      this.selectedCurrency = ShopCurrency['COINS'];
      this.selectedItemCost = this.selectedItem.sellCost.coins;
    }
    this.changeCount(this.selectedItem.count);
  }

  openSellDialog(content, itemId: string) {
    this.shopService.getSellItemCost(itemId).subscribe(data => {
      this.selectedItem.sellCost = data;
      if (this.selectedCurrency === ShopCurrency['GOLD']) {
        this.selectedItemCost = this.selectedItem.sellCost.gold;
      } else {
        this.selectedItemCost = this.selectedItem.sellCost.coins;
      }
      this.currSelectedItemCount = this.selectedItem.count;
      this.maxSelectedItemCount = this.selectedItem.count;
      this.modalService.open(content);
    });
  }

  changeCount(value: any) {
    let max = this.maxSelectedItemCount;
    if (value > max) {
      value = max;
    }
    if (value < 1) {
      value = 1;
    }
    if (this.selectedCurrency === ShopCurrency['GOLD']) {
      this.selectedItemCost = this.selectedItem.sellCost.gold * value;
    } else {
      this.selectedItemCost = this.selectedItem.sellCost.coins * value;
    }
    this.currSelectedItemCount = value;
  }

  changeSearchString(value: string) {
    this.searchItemName = value;
  }
}
