import {Component, OnInit} from '@angular/core';
import {CharacterModel} from '../../../../models/character.model';
import {CollectionService} from '../../../../services/collection/collection.service';
import {PlayerService} from '../../../../services/player/player.service';
import {AuthService} from '../../../../services/auth/auth.service';
import {DomSanitizer} from '@angular/platform-browser';
import {CharacterStarModel} from '../../../../models/character-star.model';

@Component({
  selector: 'app-profile-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  characters: CharacterModel[] = [];
  stars: CharacterStarModel[] = [];

  constructor(private colService: CollectionService, private playerService: PlayerService,
              private authService: AuthService, private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.colService.getCollection().subscribe(characters => {
      if (!characters.length) {
        return;
      }
      characters = characters.reduce(function (map, obj) {
        map[obj.id] = obj;
        return map;
      }, []);
      this.playerService.getUserCollection(this.authService.getUserId()).subscribe(userCollection => {
        userCollection.map(item => {
          characters[item.characterId].fragmentCount = item.fragmentCount;
          characters[item.characterId].starCount = item.starCount;
        });
        this.characters = Object.values(characters).sort(function (a, b) {
          return a.fragmentCount - b.fragmentCount;
        });

      });
    });

    this.colService.getStars().subscribe(stars => {
      this.stars = stars;
    });
  }

  getSanitizingImage(image: string) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

  getBorderStyle(starCount: number) {
    if (starCount === 0) {
      return '3pt groove #b3b3b3';
    }
    return '3pt groove ' + this.stars[starCount - 1].borderColor;
  }

  getNextStarNeedFragments(starCount: number) {
    if (starCount === 5) {
      return '';
    }
    return 'из ' + this.stars[starCount].needFragments;
  }

  getDescription(character: CharacterModel) {
    let treasureNames = '\n';
    for (let i = 0; i < character.treasures.length; i++) {
      treasureNames += (i + 1) + ') ' + character.treasures[i].name + '\n';
    }
    return 'Выпадает из сундуков:' + treasureNames;
  }
}
