import { Component, OnInit, AfterViewInit } from '@angular/core';
import {PlayerService} from '../../../../services/player/player.service';
import {UserEditInfoModel} from '../../../../models/user-edit-info.model';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../../../services/auth/auth.service';
declare var $: any;

@Component({
  selector: 'app-profile-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit, AfterViewInit {

  constructor(private playerService: PlayerService, private toastr: ToastrService,
              private auth: AuthService) { }
  userInfo: UserEditInfoModel = new UserEditInfoModel(
    'ХубаБуба',
    13, 13657, 17510,
    'olol@mail.ru', 'anonim-avatar.png',
    '', new Date('15.08.1987'), '', '');

  ngOnInit() {
    this.fillInfo(this.auth.getUserId());
  }

  ngAfterViewInit() {
    let $this = this;
    $('.datepicker').datepicker({
      format: 'dd.mm.yyyy',
      defaultDate: new Date($this.userInfo.birthday)
    });
  }

  saveInfo() {
    if (this.userInfo.password !== '' && this.userInfo.password !== this.userInfo.confirm_password) {
      this.toastr.error('', 'Пароли не совпадают');
      return;
    }
    this.playerService.savePlayerProfile(this.auth.getUserId(), this.userInfo).subscribe((data) => {
      location.reload();
    });
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      this.userInfo.avatar_name = event.target.files[0].name;

      const reader: FileReader = new FileReader();
      reader.onloadend = (e) => {
        this.toastr.success('', 'Изображние загружено');
        this.userInfo.avatarImage = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  private fillInfo(userId) {
    this.playerService.getPlayerFullInfo(userId).subscribe((data) => {
      this.userInfo = data;
    });
  }
}
