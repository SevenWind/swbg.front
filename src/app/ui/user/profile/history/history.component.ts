import { Component, OnInit } from '@angular/core';
import {LootboxService} from '../../../../services/lootbox/lootbox.service';
import {HistoryListModel} from '../../../../models/history-list.model';
import {PaginButtonModel} from '../../../../models/pagin-button.model';
import {HtmlUtility} from '../../../html-utility';
import {AuthService} from '../../../../services/auth/auth.service';

@Component({
  selector: 'app-profile-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  constructor(public lootboxService: LootboxService, private auth: AuthService) { }

  displayHistory: HistoryListModel = new HistoryListModel([], 0, 0);
  numberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.displayHistory, 1);

  ngOnInit() {
    this.fillHistory(1);
  }

  prev() {
    if (this.displayHistory.pageNumber - 1 < 1) {
      return;
    }
    this.fillHistory(this.displayHistory.pageNumber - 1);
  }

  next() {
    if (this.displayHistory.pageNumber + 1 > this.displayHistory.pageCount) {
      return;
    }
    this.fillHistory(this.displayHistory.pageNumber + 1);
  }

  goToPage(pageNumber) {
    this.fillHistory(pageNumber);
  }

  private fillHistory(pageNum: number) {
    this.lootboxService.getHist(this.auth.getUserId(), pageNum).subscribe((data) => {
      this.displayHistory = data;
      this.numberButtons = HtmlUtility.getPaginButtonNumbers(this.displayHistory, data.pageCount);
    });
  }
}
