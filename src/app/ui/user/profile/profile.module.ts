import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {InventoryComponent} from './inventory/inventory.component';
import {HistoryComponent} from './history/history.component';
import {CollectionComponent} from './collection/collection.component';
import {InfoComponent} from './info/info.component';
import {ProfileComponent} from './profile.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    ProfileComponent, InfoComponent, HistoryComponent, InventoryComponent, CollectionComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    FormsModule
  ],
  exports: [ProfileComponent]
})
export class ProfileModule { }
