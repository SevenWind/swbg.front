import {Component, OnInit} from '@angular/core';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {ShopModel} from '../../../models/shop.model';
import {ShopCurrency} from '../../../models/enums';
import {ShopItemModel} from '../../../models/shop-item.model';
import {ShopService} from '../../../services/shop/shop.service';
import {HtmlUtility} from '../../html-utility';
import {PaginButtonModel} from '../../../models/pagin-button.model';
import {PlayerService} from '../../../services/player/player.service';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../../services/auth/auth.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal,
              private shopService: ShopService, private playerService: PlayerService,
              private toastr: ToastrService, private auth: AuthService) {
    // конфигурация модальных окон
    config.backdrop = 'static';
    config.keyboard = true;
  }

  selectedCurrency = ShopCurrency['GOLD'];
  selectedItemCost = 0;
  selectedItem: ShopItemModel;
  shop: ShopModel = new ShopModel(
    [],
    0,
    0,
  );
  numberButtons: PaginButtonModel[] = HtmlUtility.getPaginButtonNumbers(this.shop, 1);

  ngOnInit() {
    this.fillShop(1);
  }

  prev() {
    if (this.shop.pageNumber - 1 < 1) {
      return;
    }
    this.fillShop(this.shop.pageNumber - 1);
  }

  next() {
    if (this.shop.pageNumber + 1 > this.shop.pageCount) {
      return;
    }
    this.fillShop(this.shop.pageNumber + 1);
  }

  goToPage(pageNumber) {
    this.fillShop(pageNumber);
  }

  private fillShop(pageNum: number) {
    this.shopService.getShopItems(pageNum, 24).subscribe((data) => {
      data.items = data.items.filter(a => a.isAvail);
      this.shop = data;
      this.numberButtons = HtmlUtility.getPaginButtonNumbers(this.shop, data.pageCount);
    });
  }

  openBuyDialog(content, itemId: string) {
    this.selectedItem = this.shop.items.filter(item => item.item.id === itemId)[0];
    if (this.selectedCurrency === ShopCurrency['GOLD']) {
      this.selectedItemCost = this.selectedItem.buyCost.gold;
    } else {
      this.selectedItemCost = this.selectedItem.buyCost.coins;
    }

    this.modalService.open(content);
  }

  setCurrency(currency: string) {
    if (currency === 'gold') {
      this.selectedCurrency = ShopCurrency['GOLD'];
      this.selectedItemCost = this.selectedItem.buyCost.gold;
    } else {
      this.selectedCurrency = ShopCurrency['COINS'];
      this.selectedItemCost = this.selectedItem.buyCost.coins;
    }
    this.changeCount(this.selectedItem.count);
  }

  buyItem(itemId) {
    this.modalService.dismissAll();
    const currencyType = Object.keys(ShopCurrency).filter(k => ShopCurrency[k as any] === this.selectedCurrency)[0];
    let cost = 0;
    if (this.selectedCurrency === ShopCurrency['GOLD']) {
      cost = this.selectedItem.buyCost.gold;
    } else {
      cost = this.selectedItem.buyCost.coins;
    }
    this.playerService.buyItem(this.auth.getUserId(), itemId, this.selectedItem.count, currencyType, cost).subscribe(res => {
      if (res) {
        this.toastr.success('', 'Успешная покупка предмета');
      }
    });
  }

  changeCount(value: any) {
    if (value > 100) {
      value = 100;
    }
    if (value < 1) {
      value = 1;
    }
    if (this.selectedCurrency === ShopCurrency['GOLD']) {
      this.selectedItemCost = this.selectedItem.buyCost.gold * value;
    } else {
      this.selectedItemCost = this.selectedItem.buyCost.coins * value;
    }
    this.selectedItem.count = value;
  }
}
