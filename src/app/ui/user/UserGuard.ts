import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from '../../services/auth/auth.service';
import {Role} from '../../models/enums';

@Injectable()
export class UserGuard implements CanActivate {

  constructor(protected router: Router, private auth: AuthService) {
  }

  canActivate() {
    if (this.auth.IsLoggedIn()) {
      if (this.auth.UserInRole(Role.PLAYER)) {
        return true;
      }
    }
    this.router.navigate(['signin']);
    return false;
  }
}
