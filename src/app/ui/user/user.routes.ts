import {Routes} from '@angular/router';
import {LootboxesComponent} from './lootboxes/lootboxes.component';
import {MailComponent} from './mail/mail.component';
import {RatingComponent} from './rating/rating.component';
import {CalendarComponent} from './calendar/calendar.component';
import {DonateComponent} from './donate/donate.component';
import {ShopComponent} from './shop/shop.component';
import {ProfileComponent} from './profile/profile.component';

export const USER_ROUTES: Routes = [
  { path: '', redirectTo: 'user', pathMatch: 'full' },
  { path: 'user', component: LootboxesComponent },
  { path: 'user/profile', component: ProfileComponent },
  { path: 'user/rating', component: RatingComponent },
  { path: 'user/calendar', component: CalendarComponent },
  { path: 'user/shop', component: ShopComponent },
  { path: 'user/mail', component: MailComponent },
  { path: 'user/donate', component: DonateComponent },
];
