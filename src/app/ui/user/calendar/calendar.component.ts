import { Component, OnInit } from '@angular/core';
import {CalendarService} from '../../../services/calendar/calendar.service';
import {CalendarModel} from '../../../models/calendar.model';
import {CalendarRewardModel} from '../../../models/calendar-reward.model';
import {PlayerService} from '../../../services/player/player.service';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../../services/auth/auth.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal,
              private calendarService: CalendarService, private playerService: PlayerService,
              private toastr: ToastrService, private auth: AuthService) {
    // конфигурация модальных окон
    config.backdrop = 'static';
    config.keyboard = true;
  }

  dailyReward: CalendarRewardModel = new CalendarRewardModel(1, 1, '', '', []);
  d7Reward: CalendarRewardModel = new CalendarRewardModel(1, 1, '', '', []);
  d14Reward: CalendarRewardModel = new CalendarRewardModel(1, 1, '', '', []);
  d21Reward: CalendarRewardModel = new CalendarRewardModel(1, 1, '', '', []);
  dFullReward: CalendarRewardModel = new CalendarRewardModel(1, 1, '', '', []);
  calendar: CalendarModel = new CalendarModel(
    100,
    [],
    []
  );
  selectedDayIndex = 0;
  currentDayNum = 1;

  ngOnInit() {
    this.currentDayNum = new Date().getDate();
    this.calendarService.getCalendarInfo().subscribe(data => {
      this.calendar = data;

      this.dailyReward = data.rewards.filter(a => a.code === 'DAILY')[0];
      this.d7Reward = data.rewards.filter(a => a.code === '7_DAYS')[0];
      this.d14Reward = data.rewards.filter(a => a.code === '14_DAYS')[0];
      this.d21Reward = data.rewards.filter(a => a.code === '21_DAYS')[0];
      this.dFullReward = data.rewards.filter(a => a.code === 'FULL_MONTH')[0];

      this.playerService.getCalendar(this.auth.getUserId()).subscribe(res => this.calendar.userDayChecks = res);
      this.playerService.getRewardStatuses(this.auth.getUserId()).subscribe(res => this.calendar.userCollectedRewards = res);
    });
  }

  checkDay(dayNumber) {
    this.modalService.dismissAll();
    const day = this.calendar.days[dayNumber - 1];
    const cost = dayNumber === this.currentDayNum ? 0 : this.calendar.checkOldCellGoldCost;
    this.playerService.checkDay(this.auth.getUserId(), dayNumber, cost, day).subscribe(_ => {
      this.calendarService.getCalendarInfo().subscribe(data => {
        this.calendar = data;
        this.playerService.getCalendar(this.auth.getUserId()).subscribe(res => this.calendar.userDayChecks = res);
        this.playerService.getRewardStatuses(this.auth.getUserId()).subscribe(res => this.calendar.userCollectedRewards = res);
      });
    });
  }

  collectReward(rewardId) {
    const reward = this.calendar.rewards[rewardId - 1];
    if (rewardId !== 1) {
      const checkCount = this.calendar.userDayChecks.filter(a => a).length;
      if (checkCount < reward.needCheckDaysCount) {
        this.toastr.warning('', 'Недостаточно отмеченных дней для сбора данной награды');
        return;
      }
    }
    this.playerService.collectReward(this.auth.getUserId(), reward).subscribe(_ => {
      this.calendarService.getCalendarInfo().subscribe(data => {
        this.calendar = data;
        this.playerService.getCalendar(this.auth.getUserId()).subscribe(res => this.calendar.userDayChecks = res);
        this.playerService.getRewardStatuses(this.auth.getUserId()).subscribe(res => this.calendar.userCollectedRewards = res);
      });
    });
  }

  openCheckModal(content, i: number) {
    this.selectedDayIndex = i;
    this.modalService.open(content);
  }
}
