import {Component, OnInit} from '@angular/core';
import {PlayerService} from '../../../services/player/player.service';
import {MessageBoxType} from '../../../models/enums';
import {UserShortInfoModel} from '../../../models/userShortInfo.model';
import {NavigationEnd, Router} from '@angular/router';
import {UserCashModel} from '../../../models/userCash.model';
import {MailService} from '../../../services/mail/mail.service';
import {ReplaySubject} from 'rxjs';
import {filter} from 'rxjs/internal/operators';
import {AuthService} from '../../../services/auth/auth.service';
import { HtmlUtility } from '../../html-utility';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  logo = '𝓑𝓜𝓞'; // '乃爪ㄖ';//'ℬℳØ';
  unread_message_count = 9;
  active_page_url = this.router.url;

  userCash: UserCashModel = new UserCashModel(0, 0);
  userInfo: UserShortInfoModel = new UserShortInfoModel('', '../../../assets/player_avatars/anonim-avatar.png', 1, 0, 0, 0, 1000);

  private _routerEvents = new ReplaySubject<NavigationEnd>();

  constructor(private playerService: PlayerService, private router: Router, private mailService: MailService,
              private authService: AuthService) {
    this.router.events.subscribe(this._routerEvents);
  }

  ngOnInit() {
    this.playerService.getPlayerShortInfo(this.authService.getUserId()).subscribe((data) => {
      this.userInfo = data;
      this.userCash = data.cash;
    });

    this.playerService.userCashChange.subscribe(cash => {
      this.userCash = cash;
    });

    this.playerService.userExpChange.subscribe(lvl => {
      this.userInfo.level = lvl.level;
      this.userInfo.xp = lvl.xp;
      this.userInfo.percentOfNextLvl = Math.round(100 - (lvl.xpToNextLvl - this.userInfo.xp) / lvl.xpToNextLvl * 100);
    });

    this._routerEvents
      .pipe(filter(e => e instanceof NavigationEnd))
      .subscribe((e) => {
        this.active_page_url = e.urlAfterRedirects.slice(1);
      });

    this.mailService.getMessages(this.authService.getUserId(), MessageBoxType.Incoming, 1).subscribe(_ => {
      this.unread_message_count = this.mailService.unreadedMessagesCount;
    });
  }

  getDisplayCount(value: number) {
    return HtmlUtility.formatLongNumber(value);
  }

  setActive(active_page_url) {
    this.active_page_url = active_page_url;
  }

  exitFromApp() {
    this.authService.logout().subscribe();
  }
}
