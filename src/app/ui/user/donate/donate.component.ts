import { Component, OnInit } from '@angular/core';
import {PlayerService} from '../../../services/player/player.service';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../../services/auth/auth.service';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.scss']
})
export class DonateComponent implements OnInit {

  constructor(public playerService: PlayerService, private toastr: ToastrService,
              private auth: AuthService) { }

  ngOnInit() {
  }

  donate(value) {
    this.playerService.donate(this.auth.getUserId(), value).subscribe((data) => {
      this.toastr.success('', 'Кошелек успешно пополнен на ' + value + ' золота!');
    });
  }
}
