import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth/auth.service';
import {ToastrService} from 'ngx-toastr';
import moment from '../../../../../node_modules/moment/moment';
import {Router} from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, AfterViewInit {

  constructor(private authService: AuthService, private toastr: ToastrService,
              private router: Router) {
  }

  regForm = {
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
    birthdate: '',
  };

  ngOnInit() {
  }

  ngAfterViewInit() {
    $('.datepicker').datepicker({
      format: 'dd.mm.yyyy',
    });
  }

  register() {
    const errors = [];
    if (this.regForm.birthdate === '') {
      errors.push('Не заполнена дата рождения');
    }
    if (this.regForm.name === '') {
      errors.push('Не заполнено имя');
    }
    if (this.regForm.name.length > 12) {
      errors.push('Длина имени не должна быть больше 12 символов');
    }
    if (this.regForm.email === '') {
      errors.push('Не заполнена почта');
    }
    if (this.regForm.password === '') {
      errors.push('Не заполнен пароль');
    }
    if (this.regForm.confirmPassword === '') {
      errors.push('Не заполнено поле подтверждения пароля');
    }
    if (errors.length) {
      for (const error of errors) {
        this.toastr.error('', error);
      }
      return;
    }

    let today = moment();
    let date18 = today.add('year', -18);
    if (new Date(this.regForm.birthdate).toString() == 'Invalid Date') {
      this.toastr.error('Воспользуйтесь окном выбора даты, появляющимся при клике на поле даты', 'Дата имеет некорректный формат');
      return;
    }
    if (date18 <= moment(this.regForm.birthdate)) {
      this.toastr.error('', 'К регистрации допускаются только лица, достигшие 18 лет');
      return;
    }

    this.authService.signUp(this.regForm).subscribe(res => {
      this.router.navigate(['signin']);
    });
  }

}
