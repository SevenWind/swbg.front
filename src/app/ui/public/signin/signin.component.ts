import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  loginForm = {
    email: '',
    password: '',
    rememberMe: false
  };

  constructor(private authService: AuthService) {

  }

  ngOnInit() {

  }

  login() {
    this.authService.auth(this.loginForm).subscribe();
  }
}
