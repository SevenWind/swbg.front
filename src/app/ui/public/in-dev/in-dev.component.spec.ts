import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InDevComponent } from './in-dev.component';

describe('InDevComponent', () => {
  let component: InDevComponent;
  let fixture: ComponentFixture<InDevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InDevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InDevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
