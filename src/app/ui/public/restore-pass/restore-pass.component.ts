import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth/auth.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-restore-pass',
  templateUrl: './restore-pass.component.html',
  styleUrls: ['./restore-pass.component.scss']
})
export class RestorePassComponent implements OnInit {

  constructor(private authService: AuthService, private toastr: ToastrService) {
  }

  email: string;

  ngOnInit() {
  }

  restorePass() {
    if (this.email === '') {
      this.toastr.error('', 'Введите вашу почту');
      return;
    }
    this.authService.restorePassword(this.email).subscribe(res => {
        if (res) {
          this.toastr.info('', 'Вскоре на вашу почту должно придти письмо.');
        }
      }
    );
  }

  reSendEmail() {
    if (this.email === '') {
      this.toastr.error('', 'Введите вашу почту');
      return;
    }
    this.authService.reSendEmailConfirm(this.email).subscribe(res => {
        if (res) {
          this.toastr.info('', 'Вскоре на вашу почту должно придти письмо.');
        }
      }
    );
  }
}
