import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../material-import';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { SigninComponent } from './public/signin/signin.component';
import { LayoutComponent } from './user/layout/layout.component';
import { HeaderComponent } from './user/header/header.component';
import { FooterComponent } from './user/footer/footer.component';
import { LootboxesComponent } from './user/lootboxes/lootboxes.component';
import {ProfileModule} from './user/profile/profile.module';
import { RatingComponent } from './user/rating/rating.component';
import { DonateComponent } from './user/donate/donate.component';
import { CalendarComponent } from './user/calendar/calendar.component';
import { ShopComponent } from './user/shop/shop.component';
import { MailComponent } from './user/mail/mail.component';
import {AdminComponent} from './admin/admin.component';
import {UserComponent} from './user/user.component';
import {AppRoutingModule} from '../app-routing.module';
import { AdminLayoutComponent } from './admin/admin-layout/admin-layout.component';
import { AdminHeaderComponent } from './admin/admin-header/admin-header.component';
import { AdminFooterComponent } from './admin/admin-footer/admin-footer.component';
import { UsersComponent } from './admin/users/users.component';
import {ToastrModule} from 'ngx-toastr';
import { SignupComponent } from './public/signup/signup.component';
import { RestorePassComponent } from './public/restore-pass/restore-pass.component';
import { InDevComponent } from './public/in-dev/in-dev.component';
import {AdminLootboxeComponent} from './admin/lootboxes/lootboxes.component';
import { CalendarConfigComponent } from './admin/calendar-config/calendar-config.component';
import { AdminShopComponent } from './admin/admin-shop/admin-shop.component';

@NgModule({
  declarations: [
    LayoutComponent, HeaderComponent, FooterComponent,
    LootboxesComponent,
    RatingComponent,
    DonateComponent,
    CalendarComponent,
    ShopComponent,
    MailComponent,
    SigninComponent,
    UserComponent,
    AdminComponent,
    AdminLayoutComponent,
    AdminHeaderComponent,
    AdminFooterComponent,
    UsersComponent,
    AdminLootboxeComponent,
    SignupComponent,
    RestorePassComponent,
    InDevComponent,
    CalendarConfigComponent,
    AdminShopComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    ProfileModule,
    MaterialModule,
    AppRoutingModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-right',
      maxOpened: 10,
    })
  ],
  exports: [UserComponent, AdminComponent]
})
export class UiModule { }
