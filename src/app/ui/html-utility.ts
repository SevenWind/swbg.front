import {PaginButtonModel} from '../models/pagin-button.model';

export class HtmlUtility {

  public static formatLongNumber(val: number) {
    if (val < 1000) {
      return val + '';
    }

    if (val < 999999) {
      return Math.round(val / 1000) + 'k';
    }

    if (val < 999999999) {
      return Math.round(val / 1000000) + 'kk';
    }

    if (val < 999999999999) {
      return Math.round(val / 1000000000) + 'b';
    }

    return '∞';
  }

  public static getPaginButtonNumbers(paginModel, num) {
    let nums = Array(num).fill('').map((x, i) => new PaginButtonModel(
      i + 1 + '',
      paginModel.pageNumber === i + 1 ? 'active' : '',
      paginModel.pageNumber === i + 1));
    if (num <= 6) {
      return nums;
    }
    const centerNumber = paginModel.pageNumber - 2 >= 1 ? paginModel.pageNumber - 2 : 1;
    nums = nums.slice(centerNumber - 1, centerNumber - 1 + 5);
    return nums;
  }
}
