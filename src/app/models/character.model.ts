import {Rarity} from './enums';
import {TreasureModel} from './treasure.model';

export class CharacterModel {
  id: number;
  name: string;
  image: string;
  startStar: number;
  rarity: Rarity;
  treasures: TreasureModel[];
  fragmentCount: number = 0;
  starCount: number = 0;

  constructor(id: number, name: string, image: string, startStar: number, rarity: Rarity,
              treasures: TreasureModel[]) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.startStar = startStar;
    this.rarity = rarity;
    this.treasures = treasures;
  }
}
