import {ItemModel} from './item.model';
import {HtmlUtility} from '../ui/html-utility';

export class DropModel {
  item: ItemModel;
  count: number;
  displayCount: string;
  exp: number;

  /**
   * Item
   * @param item item that drop
   * @param exp exp for take from treasure
   * @param count count of items that drop
   */
  constructor(item: ItemModel, count: number, exp: number = 0) {
    this.item = item;
    this.count = count;
    this.exp = exp;
    this.displayCount = HtmlUtility.formatLongNumber(count);
  }
}
