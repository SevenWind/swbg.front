export class RatingItemModel {
  position: number;
  nick: string;
  level: number;
  currentXp: number;
  xpToLvl: number;

  constructor(position: number, nick: string, level: number, currentXp: number, xpToLvl: number) {
    this.position = position;
    this.nick = nick;
    this.level = level;
    this.currentXp = currentXp;
    this.xpToLvl = xpToLvl;
  }
}

