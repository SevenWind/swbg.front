import {ShopItemModel} from './shop-item.model';
import {Pagination} from '../interfaces/pagination.interface';

export class ShopModel implements Pagination {
  items: ShopItemModel[];
  emptyItems = [];
  pageCount: number = 0;
  pageNumber: number = 0;

  constructor(items: ShopItemModel[], pageCount: number = 0, pageNumber: number = 0, pageSize: number = 24) {
    this.pageCount = pageCount;
    this.pageNumber = pageNumber;
    this.items = items;
    if (items.length < pageSize) {
      this.emptyItems = Array(pageSize - items.length).fill('');
    }
  }
}

