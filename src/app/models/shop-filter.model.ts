import { SortDirection } from './enums';

export class ShopFilterModel {
    name = '';
    isAvail = 0;

    nameSort: SortDirection = 0; // -1: desc 1: asc 0: nosort
    buyGoldCost: SortDirection = 0;
    buyCoinsCost: SortDirection = 0;
    sellGoldCost: SortDirection = 0;
    sellCoinsCost: SortDirection = 0;
}