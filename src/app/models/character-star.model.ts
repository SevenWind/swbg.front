export class CharacterStarModel {
  star: number;
  needFragments: number;
  borderColor: string;

  constructor(star: number, fragmentCount: number, borderColor: string) {
    this.star = star;
    this.needFragments = fragmentCount;
    this.borderColor = borderColor;
  }
}
