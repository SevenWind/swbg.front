import {UserCashModel} from './userCash.model';

export class UserShortInfoModel {
  username: string;
  avatar_path: string;
  level: number;
  xp: number;
  percentOfNextLvl: number;
  cash: UserCashModel;
  avatarImage: string | ArrayBuffer;

  constructor(name: string, avatarImage: string, lvl: number, xp: number, coins: number, gold: number, nextLvlXp: number) {
    this.username = name;
    this.avatarImage = avatarImage;
    this.xp = xp;
    this.level = lvl;
    this.cash = new UserCashModel(coins, gold);
    this.percentOfNextLvl = Math.round(100 - (nextLvlXp - xp) / nextLvlXp * 100);
  }
}
