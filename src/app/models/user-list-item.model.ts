import {UserStatus} from './enums';

export class UserListItemModel {
  id: string;
  name: string;
  lvl: number;
  email: string;
  regDate: Date;
  lastOnlineDate: Date;
  isLocked: boolean;
  onlineStatus: UserStatus;
  isChecked: boolean = false;

  constructor(id: string, name: string, lvl: number, email: string,
              regDate: Date, lastOnlineDate: Date, isLocked: boolean) {
    this.id = id;
    this.name = name;
    this.lvl = lvl;
    this.email = email;
    this.regDate = regDate;
    this.lastOnlineDate = lastOnlineDate;
    this.isLocked = isLocked;
    this.onlineStatus = this.setOnlineStatus();
  }

  public get onlineIndicatorClass() {
    if (this.onlineStatus === UserStatus.В_сети) {
      return '';
    }
    if (this.onlineStatus === UserStatus.Недавно_был_в_сети) {
      return 'yellow';
    }
    return 'gray';
  }

  public get onlineStatusText() {
    if (this.onlineStatus === UserStatus.В_сети) {
      return 'В сети';
    }
    if (this.onlineStatus === UserStatus.Недавно_был_в_сети) {
      return 'Недавно был в сети';
    }
    if (this.onlineStatus === UserStatus.Заблокирован) {
      return 'Заблокирован';
    }
    return 'Не в сети';
  }

  private setOnlineStatus() {
    const today = new Date();
    const diffMs: any = (today.getTime() - new Date(this.lastOnlineDate).getTime());
    const diffMinutes: number = Math.round(diffMs / 60000);
    if (this.isLocked) {
      return UserStatus.Заблокирован;
    }
    if (diffMinutes <= 5) {
      return UserStatus.В_сети;
    }
    if (diffMinutes <= 15) {
      return UserStatus.Недавно_был_в_сети;
    }
    return UserStatus.Не_в_сети;
  }
}
