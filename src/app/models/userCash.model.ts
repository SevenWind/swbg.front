import {HtmlUtility} from '../ui/html-utility';

export class UserCashModel {
  coins: number;
  gold: number;
  displayCoins: string;

  constructor(coins: number, gold: number) {
    this.coins = coins;
    this.gold = gold;
    this.displayCoins = HtmlUtility.formatLongNumber(coins);
  }
}
