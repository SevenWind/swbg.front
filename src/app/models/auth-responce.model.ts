export class AuthResponceModel {
  user_roles: string;
  user_id: string;
  access_token: string;
  user_name: string;

  constructor(user_roles: string, user_id: string, access_token: string, user_name: string) {
    this.access_token = access_token;
    this.user_id = user_id;
    this.user_roles = user_roles;
    this.user_name = user_name;
  }
}
