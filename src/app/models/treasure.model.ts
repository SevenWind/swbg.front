import { ResourceModel } from './resource.model';

export class TreasureModel {
  id: string;
  name: string;
  code: string;
  image: string | ArrayBuffer;
  resources: ResourceModel[] = [];
  exp: number;
  isLocked: boolean;

  emptyResources = [];
  resourcesMaxSlotCount = 4;
  imgName: string;

  /**
   * Treasure constructor
   * @param id внешний идентификатор сундука
   * @param name name of treasure
   * @param code code of treasure
   * @param ress array of resources that need for opening
   * @param imagePath path to treasure image
   */
  constructor(id: string, name: string, code: string, ress: ResourceModel[], imagePath: string, exp: number, isLocked: boolean = false) {
    this.id = id;
    this.name = name;
    this.code = code;
    this.resources = ress;
    this.image = imagePath;
    this.exp = exp;
    this.isLocked = isLocked;

    if (this.resources.length < this.resourcesMaxSlotCount) {
      this.emptyResources = Array(this.resourcesMaxSlotCount - this.resources.length).fill('');
    }
  }

  public get onlineIndicatorClass() {
    if (this.isLocked) {
      return 'gray';
    }
    return '';
  }

  public get onlineStatusText() {
    if (this.isLocked) {
      return 'Не доступен';
    }
    return 'Доступен';
  }
}
