import {CalendarRewardModel} from './calendar-reward.model';
import {DropModel} from './drop.model';

export class CalendarModel {
  checkOldCellGoldCost: number;
  rewards: CalendarRewardModel[];
  days: DropModel[];
  userDayChecks: boolean[];
  userCollectedRewards: boolean[];

  constructor(checkOldCellGoldCost: number, rewards: CalendarRewardModel[], days: DropModel[]) {
    this.checkOldCellGoldCost = checkOldCellGoldCost;
    this.rewards = rewards;
    this.days = days;
  }
}
