import { ItemModel } from './item.model';

export class ResourceModel {
  item: ItemModel;
  value: number;
  isDeleted = false;

  /**
   * Resource constructor
   * @param item item
   * @param value count of resource
   */
  constructor(item: ItemModel, value: number) {
    this.item = item;
    this.value = value;
  }
}
