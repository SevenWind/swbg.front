import {Pagination} from '../interfaces/pagination.interface';
import {UserListItemModel} from './user-list-item.model';
import {UserFilterModel} from './user-filter.model';

export class UsersListModel implements Pagination {
  pageCount: number;
  pageNumber: number;
  users: UserListItemModel[];
  filter: UserFilterModel = new UserFilterModel();

  constructor(users: UserListItemModel[], filter: UserFilterModel, pageCount: number, pageNumber: number) {
    this.users = users;
    this.filter = filter;
    this.pageCount = pageCount;
    this.pageNumber = pageNumber;
  }
}
