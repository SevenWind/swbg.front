export enum Rarity {
  NONE = 1,
  COMMON,
  UNCOMMON,
  RARE,
}

export enum ItemDropFrequency {
  COMMON = 1,
  UNCOMMON = 10,
  RARE = 50,
}

export enum SortDirection {
  DESC = -1,
  NONE = 0,
  ASC = 1
}

export enum ShopCurrency {
  GOLD = 'Золота',
  COINS = 'Монет'
}

export enum Role {
  ADMIN = 'Admin',
  PLAYER = 'Player',
  TESTER = 'Tester'
}

export enum MessageBoxType {
  Incoming = 0,
  Outgoing = 1
}

export enum UserStatus {
  Все = -1,
  В_сети = 0,
  Недавно_был_в_сети,
  Не_в_сети,
  Заблокирован
}
