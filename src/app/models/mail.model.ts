import {DropModel} from './drop.model';

export class MailModel {
  id: string;
  fromId: string;
  fromName: string;
  toId: string;
  toName: string;
  theme: string;
  date: Date;
  message: string;
  isReaded: boolean;
  attachmentCollected: boolean;
  items: DropModel[];

  emptyItems: any[];
  maxItemsCount: number = 12;
  isChecked: boolean;

  constructor(id: string, fromId: string, fromName: string, toId: string, toName: string, theme: string,
              date: Date, message: string, isReaded: boolean, attachmentCollected: boolean, attachment: DropModel[]) {
    this.id = id;
    this.fromId = fromId;
    this.fromName = fromName;
    this.toId = toId;
    this.toName = toName;
    this.theme = theme;
    this.date = date;
    this.message = message;
    this.isReaded = isReaded;
    this.attachmentCollected = attachmentCollected;
    this.items = attachment;
    if (this.items.length < this.maxItemsCount) {
      this.emptyItems = Array(this.maxItemsCount - this.items.length).fill('');
    }
  }
}
