import {Rarity} from './enums';

export class HistoryModel {
  id: number;
  date: Date;
  treasure_name: string;
  item: string;
  count: string;
  rarity: Rarity;
  imagePath: string;

  constructor(id: number, date: Date, treasure_name: string, item: string, imagePath: string, count: string, rarity: Rarity) {
    this.id = id;
    this.date = date;
    this.treasure_name = treasure_name;
    this.item = item;
    this.count = count;
    this.rarity = rarity;
    this.imagePath = imagePath;
  }
}
