import {HistoryModel} from './history.model';
import {Pagination} from '../interfaces/pagination.interface';

export class HistoryListModel implements Pagination {
  history: HistoryModel[] = [];
  pageCount: number = 0;
  pageNumber: number = 0;

  constructor(history: HistoryModel[], pageCount: number, pageNumber: number) {
    this.history = history;
    this.pageNumber = pageNumber;
    this.pageCount = pageCount;
  }
}
