export class UserXpInfoModel {
  level: number;
  xpToNextLvl: number;
  xp: number;

  constructor(level: number, xp: number, xpToNextLvl: number) {
    this.level = level;
    this.xp = xp;
    this.xpToNextLvl = xpToNextLvl;
  }
}
