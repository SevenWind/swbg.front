export class UserCollectionItemModel {
  id: number;
  characterId: number;
  fragmentCount: number;
  starCount: number;

  constructor(id: number, characterId: number, fragmentCount: number, starCount: number) {
    this.id = id;
    this.characterId = characterId;
    this.fragmentCount = fragmentCount;
    this.starCount = starCount;
  }
}
