import {HtmlUtility} from '../ui/html-utility';

export class ItemCostModel {
  gold: number;
  coins: number;
  coinsDisplay: string;

  constructor(gold: number, coins: number) {
    this.gold = gold;
    this.coins = coins;
    this.coinsDisplay = HtmlUtility.formatLongNumber(coins);
  }
}
