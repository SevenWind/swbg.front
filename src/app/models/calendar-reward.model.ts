import {DropModel} from './drop.model';

export class CalendarRewardModel {
  id: number;
  needCheckDaysCount: number;
  name: string;
  code: string;
  items: DropModel[];
  emptyItems: any[] = [];

  rewardSize: number = 6;

  constructor(id: number, needCheckDaysCount: number, name: string, code: string, items: DropModel[]) {
    this.id = id;
    this.needCheckDaysCount = needCheckDaysCount;
    this.name = name;
    this.code = code;
    this.items = items;
    if (items.length < this.rewardSize) {
      this.emptyItems = Array(this.rewardSize - items.length).fill('');
    }
  }
}
