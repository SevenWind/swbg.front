import moment from '../../../node_modules/moment/moment';

export class UserEditInfoModel {
  name: string;
  level: number;
  xp: number;
  nextXp: number;
  email: string;
  avatar_name: string;
  current_password: string;
  password: string;
  confirm_password: string;
  birthday: string;

  avatarImage: string | ArrayBuffer;

  constructor(name: string, level: number, currXp: number, nextXp: number,
              email: string, avatar_name: string, avatarImage: string, birthday: Date, current_password: string, password: string) {
    this.name = name;
    this.email = email;
    this.birthday = moment(birthday).format('DD.MM.YYYY');
    this.password = password;
    this.level = level;
    this.xp = currXp;
    this.nextXp = nextXp;
    this.current_password = current_password;
    this.avatar_name = avatar_name;

  }
}
