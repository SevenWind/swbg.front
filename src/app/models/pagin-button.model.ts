export class PaginButtonModel {
  value: string;
  cls: string;
  isDisabled: boolean;

  constructor(value: string, cls: string, isDisabled: boolean) {
    this.value = value;
    this.cls = cls;
    this.isDisabled = isDisabled;
  }
}
