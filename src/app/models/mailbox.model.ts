import {Pagination} from '../interfaces/pagination.interface';
import {MailModel} from './mail.model';

export class MailBoxModel implements Pagination {
  pageCount: number;
  pageNumber: number;
  mails: MailModel[];

  constructor(mails: MailModel[], pageNumber: number, pageCount: number) {
    this.mails = mails;
    this.pageNumber = pageNumber;
    this.pageCount = pageCount;
  }
}
