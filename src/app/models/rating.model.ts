import {RatingItemModel} from './rating-item.model';
import {Pagination} from '../interfaces/pagination.interface';

export class RatingModel implements Pagination {
  rating: RatingItemModel[];
  pageCount: number = 0;
  pageNumber: number = 0;

  constructor(rating: RatingItemModel[], pageCount: number, pageNumber: number) {
    this.rating = rating;
    this.pageCount = pageCount;
    this.pageNumber = pageNumber;
  }
}
