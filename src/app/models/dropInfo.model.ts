import { ItemModel } from './item.model';
import {ItemDropFrequency} from './enums';

export class DropInfoModel {
  item: ItemModel;
  chance: number;
  count: number;
  frequency: ItemDropFrequency;
  exp: number;
  isDeleted = false;

  /**
   * Item
   * @param item item that drop
   * @param frequency how often this item can drop. each, one time from 10, one time from 50
   * @param chance chance of drop
   * @param dropCount count of items that drop
   * @param exp exp for getting
   */
  constructor(item: ItemModel, frequency: ItemDropFrequency, chance: number, dropCount: number, exp: number) {
    this.item = item;
    this.frequency = frequency;
    this.chance = chance;
    this.count = dropCount;
    this.exp = exp;
  }
}
