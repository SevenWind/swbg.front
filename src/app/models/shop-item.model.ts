import {ItemCostModel} from './item-cost.model';
import {DropModel} from './drop.model';
import {Rarity} from './enums';
import {ItemModel} from './item.model';

export class ShopItemModel {
  item: ItemModel;
  count: number;
  buyCost: ItemCostModel;
  sellCost: ItemCostModel;
  isAvail: boolean;
  limit: number;

  constructor(id: string, name: string, code: string, rarity: Rarity, imagePath: string,
              count: number, buyCost: ItemCostModel, sellCost: ItemCostModel,
              isAvail: boolean, limit: number = -1) {
    this.item = new ItemModel(id, name, rarity, imagePath, code);
    this.count = count;
    this.buyCost = buyCost;
    this.sellCost = sellCost;
    this.isAvail = isAvail;
    this.limit = limit;
  }
}
