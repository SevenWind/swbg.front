import { Rarity } from './enums';

export class ItemModel {
  id: string;
  name: string;
  code: string;
  image: string | ArrayBuffer;
  rarity: Rarity;

  /**
   * Item
   * @param id id
   * @param name item name
   * @param rarity item rarity
   * @param imagePath item icon\image
   * @param name item name
   */
  constructor(id: string, name: string, rarity: Rarity, imagePath: string, code: string = '') {
    this.id = id;
    this.name = name;
    this.rarity = rarity;
    this.image = imagePath;
    this.code = code;
  }
}
