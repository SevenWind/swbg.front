import {SortDirection, UserStatus} from './enums';

export class UserFilterModel {
  name: string;
  fromLvl: number;
  toLvl: number;
  email: string;
  fromRegDate: Date;
  toRegDate: Date;
  status: UserStatus;

  nameSort: SortDirection; // -1: desc 1: asc 0: nosort
  lvlSort: SortDirection;
  emailSort: SortDirection;
  regDateSort: SortDirection;
  lastActivitySort: SortDirection;

  constructor(name: string = '', fromLvl: number = 0, toLvl: number = 0, email: string = '',
              fromRegDate: Date = null, toRegDate: Date = null, status: UserStatus = -1,
              nameSort: SortDirection = 0, lvlSort: SortDirection = 0, emailSort: SortDirection = 0,
              regDateSort: SortDirection = 0, lastActivitySort: SortDirection = 0) {
    this.name = name;
    this.fromLvl = fromLvl;
    this.toLvl = toLvl;
    this.email = email;
    this.fromRegDate = fromRegDate;
    this.toRegDate = toRegDate;
    this.status = status;
    this.nameSort = nameSort;
    this.lvlSort = lvlSort;
    this.emailSort = emailSort;
    this.regDateSort = regDateSort;
    this.lastActivitySort = lastActivitySort;
  }
}
