import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SigninComponent} from './ui/public/signin/signin.component';
import {USER_ROUTES} from './ui/user/user.routes';
import {ADMIN_ROUTES} from './ui/admin/admin.routes';
import {AdminComponent} from './ui/admin/admin.component';
import {UserComponent} from './ui/user/user.component';
import {AdminGuard} from './ui/admin/AdminGuard';
import {UserGuard} from './ui/user/UserGuard';
import {SignupComponent} from './ui/public/signup/signup.component';
import {RestorePassComponent} from './ui/public/restore-pass/restore-pass.component';
import {InDevComponent} from './ui/public/in-dev/in-dev.component';

const routes: Routes = [
  { path: '', redirectTo: 'signin', pathMatch: 'full' },
  { path: 'restore', component: RestorePassComponent },
  { path: 'inDev', component: InDevComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'signin', component: SigninComponent },
  { path: '', component: UserComponent, canActivate: [UserGuard], children: USER_ROUTES },
  { path: '', component: AdminComponent, canActivate: [AdminGuard], children: ADMIN_ROUTES }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
