import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/internal/operators';
import { Observable } from 'rxjs';
import { ServiceUtility } from '../service-utility';
import { ShopModel } from '../../models/shop.model';
import { ShopItemModel } from '../../models/shop-item.model';
import { ItemCostModel } from '../../models/item-cost.model';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth/auth.service';
import { Config } from '../../app-config';
import { ShopFilterModel } from 'src/app/models/shop-filter.model';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  constructor(private http: HttpClient, private toastr: ToastrService,
    private auth: AuthService, private config: Config) { }

  private readonly endpoint = this.config.config.SHOP_SERVICE_URL + '/api/v1/shop/';
  private readonly adminEndpoint = this.config.config.SHOP_SERVICE_URL + '/api/v1/admin/';

  getShopItems(pageNumber, pageSize = 10,
    filter: ShopFilterModel = new ShopFilterModel()): Observable<ShopModel> {

    let filter_name = filter.name;
    if (filter_name.trim() === '') {
      filter_name = '_';
    }
    return this.http.post(this.endpoint + 'GetShopItems/pageNumber=' + pageNumber + '&pageSize=' + pageSize,
      JSON.stringify({
        name: filter_name,
        isAvail: filter.isAvail,
        nameSort: filter.nameSort,
        buyGoldCost: filter.buyGoldCost,
        buyCoinsCost: filter.buyCoinsCost,
        sellGoldCost: filter.sellGoldCost,
        sellCoinsCost: filter.sellCoinsCost,
      }),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(responce => {
          const data = ServiceUtility.extractRequestData(responce);
          return new ShopModel(
            data.shopItems.map(shopItem => new ShopItemModel(
              shopItem.item.id,
              shopItem.item.name,
              shopItem.item.code,
              shopItem.item.rarity.code,
              shopItem.item.image,
              shopItem.count,
              new ItemCostModel(shopItem.buyGoldCost, shopItem.buyCoinCost),
              new ItemCostModel(shopItem.sellGoldCost, shopItem.sellCoinsCost),
              shopItem.isAvail,
              shopItem.limit
            )
            ),
            data.pageCount,
            data.pageNumber);
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('getShopItems', this.toastr)));
  }

  getAllShopItems(pageNumber, pageSize = 10,
    filter: ShopFilterModel = new ShopFilterModel()): Observable<ShopModel> {

    let filter_name = filter.name;
    if (filter_name.trim() === '') {
      filter_name = '_';
    }
    return this.http.post(this.endpoint + 'GetAllShopItems/pageNumber=' + pageNumber + '&pageSize=' + pageSize,
      JSON.stringify({
        name: filter_name,
        isAvail: filter.isAvail,
        nameSort: filter.nameSort,
        buyGoldCost: filter.buyGoldCost,
        buyCoinsCost: filter.buyCoinsCost,
        sellGoldCost: filter.sellGoldCost,
        sellCoinsCost: filter.sellCoinsCost,
      }),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(responce => {
          const data = ServiceUtility.extractRequestData(responce);
          return new ShopModel(
            data.shopItems.map(shopItem => new ShopItemModel(
              shopItem.item.id,
              shopItem.item.name,
              shopItem.item.code,
              shopItem.item.rarity.code,
              shopItem.item.image,
              shopItem.count,
              new ItemCostModel(shopItem.buyGoldCost, shopItem.buyCoinCost),
              new ItemCostModel(shopItem.sellGoldCost, shopItem.sellCoinsCost),
              shopItem.isAvail,
              shopItem.limit
            )
            ),
            data.pageCount,
            data.pageNumber);
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('getShopItems', this.toastr)));
  }

  getItems(pageNumber, pageSize = 10): Observable<ShopModel> {
    return this.http.get(this.endpoint + 'GetItems/pageNumber=' + pageNumber + '&pageSize=' + pageSize,
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(responce => {
          const data = ServiceUtility.extractRequestData(responce);
          return new ShopModel(
            data.shopItems.map(shopItem => new ShopItemModel(
              shopItem.id,
              shopItem.name,
              shopItem.code,
              shopItem.rarity.code,
              shopItem.image,
              0,
              new ItemCostModel(0, 0),
              new ItemCostModel(0, 0),
              true
            )
            ),
            data.pageCount,
            data.pageNumber);
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('getShopItems', this.toastr)));
  }

  getSellItemCost(itemId): Observable<ItemCostModel> {
    return this.http.get(this.endpoint + 'GetSellItemCost/' + itemId, ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return new ItemCostModel(data.gold, data.coins);
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getSellItemCost', this.toastr)));
  }

  changeLockItem(id: string, islock: boolean): Observable<boolean> {
    return this.http.post(this.adminEndpoint + 'ChangeLockItem/itemId=' + id + '&isLock=' + islock, 
      JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          console.log(_);
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('changeLockItem', this.toastr, false)));
  }

  deleteItem(itemId: string): Observable<boolean> {
    return this.http.post(this.adminEndpoint + 'DeleteItem/' + itemId, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('deleteItem', this.toastr, false)));
  }

  restoreItem(itemId: string): Observable<boolean> {
    return this.http.post(this.adminEndpoint + 'RestoreItem/' + itemId, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('restoreItem', this.toastr, false)));
  }

  saveItem(item: ShopItemModel): Observable<string> {
    item.item.id = item.item.id === '' ? '00000000-0000-0000-0000-000000000000' : item.item.id;
    return this.http.post(this.adminEndpoint + 'SaveItem/', JSON.stringify({
      count: item.count,
      limit: item.limit,
      buyGoldCost: item.buyCost.gold,
      buyCoinCost: item.buyCost.coins,
      sellGoldCost: item.sellCost.gold,
      sellCoinsCost: item.sellCost.coins,
      item_id: item.item.id,
      item: {
        id: item.item.id,
        name: item.item.name,
        code: item.item.code,
        image: item.item.image,
        rarity_id: item.item.rarity
      }
    }),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(res => {
          return res;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('saveItem', this.toastr, '00000000-0000-0000-0000-000000000000')));
  }

}
