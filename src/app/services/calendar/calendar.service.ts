import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/internal/operators';
import { ServiceUtility } from '../service-utility';
import { CalendarModel } from '../../models/calendar.model';
import { CalendarRewardModel } from '../../models/calendar-reward.model';
import { DropModel } from '../../models/drop.model';
import { ItemModel } from '../../models/item.model';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth/auth.service';
import { Config } from '../../app-config';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  constructor(private http: HttpClient, private toastr: ToastrService,
    private auth: AuthService, private config: Config) {
  }

  readonly endpoint = this.config.config.ADMIN_SERVICE_URL + '/api/v1/calendar/';
  readonly adminEndpoint = this.config.config.ADMIN_SERVICE_URL + '/api/v1/admin/';

  getCalendarInfo(): Observable<CalendarModel> {
    return this.http.get(this.endpoint + 'GetCalendarInfo', ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return new CalendarModel(
          data.checkOldCellGoldCost,
          data.rewards.map(reward => new CalendarRewardModel(
            reward.id,
            reward.needCheckDaysCount,
            reward.name,
            reward.code,
            reward.items.map(item => new DropModel(
              new ItemModel(
                item.item.id,
                item.item.name,
                item.item.rarity.code,
                ServiceUtility.calcImageUrl(item.item.image, '../../../assets/'),
                ),
              item.count
            ))
          )
          ),
          data.days.map(day => new DropModel(
            new ItemModel(day.item.id, day.item.name, day.item.rarity.code,
              ServiceUtility.calcImageUrl(day.item.image, '../../../assets/')),
            day.count
          ))
        );
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getShopItems', this.toastr)));
  }

  saveCalendarCellReward(dayNumber: number, item: DropModel): Observable<boolean> {
    return this.http.post(this.adminEndpoint + 'SaveCalendarCellReward/day=' + dayNumber, JSON.stringify({
      item_id: item.item.id,
      count: item.count
    }), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(_ => {
      return _;
    }, catchError(ServiceUtility.handleErrorWithToast<any>('saveCalendarCellReward', this.toastr, false)));
  }

  removeReward(rewardId: number, item: DropModel): Observable<boolean> {
    return this.http.post(this.adminEndpoint + 'DeleteRewardItem/rewardId=' + rewardId + '&itemId=' + item.item.id, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(_ => {
        return _;
      }, catchError(ServiceUtility.handleErrorWithToast<any>('removeReward', this.toastr, false)));
  }

  addReward(rewardId: number, item: DropModel): Observable<boolean> {
    return this.http.post(this.adminEndpoint + 'AddRewardItem/rewardId=' + rewardId +
      '&itemId=' + item.item.id + '&count=' + item.count, JSON.stringify({}), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(_ => {
        return _;
      }, catchError(ServiceUtility.handleErrorWithToast<any>('addReward', this.toastr, false)));
  }

  saveCheckOldCellGoldCost(cost: number) {
    return this.http.post(this.adminEndpoint + 'SaveCheckOldCellGoldCost/cost=' + cost,
      JSON.stringify({}), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(_ => {
        return _;
      }, catchError(ServiceUtility.handleErrorWithToast<any>('saveCheckOldCellGoldCost', this.toastr, false)));
  }

  restoreItem(itemId: string): any {
    return this.http.post(this.adminEndpoint + 'RestoreItem/' + itemId,
      JSON.stringify({}), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(map(res => {
        return true;
      }), catchError(ServiceUtility.handleErrorWithToast<any>('restoreItem', this.toastr, false)));
  }
  deleteItem(itemId: string): any {
    return this.http.post(this.adminEndpoint + 'DeleteItem/' + itemId,
      JSON.stringify({}), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(map(res => {
        return true;
      }), catchError(ServiceUtility.handleErrorWithToast<any>('deleteItem', this.toastr, false)));
  }

  saveItem(item: ItemModel): any {
    item.id = item.id === '' ? '00000000-0000-0000-0000-000000000000' : item.id;
    return this.http.post(this.adminEndpoint + 'SaveItem/', JSON.stringify({
      id: item.id,
      name: item.name,
      code: item.code,
      image: item.image,
      rarity_id: item.rarity
    }),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('saveItem', this.toastr, false)));
  }
}
