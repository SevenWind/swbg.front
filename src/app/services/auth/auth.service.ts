import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ServiceUtility} from '../service-utility';
import {catchError, tap, map} from 'rxjs/internal/operators';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {AuthResponceModel} from '../../models/auth-responce.model';
import {Role} from '../../models/enums';
import {Config} from '../../app-config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router,
              private toastr: ToastrService, private config: Config) {
  }

  readonly authEndpoint = this.config.config.USER_SERVICE_URL + '/api/v1/auth/';

  auth(loginForm: { email: string; password: string; rememberMe: boolean }) {
    return this.http.post(this.authEndpoint + 'SignIn/', JSON.stringify(loginForm), ServiceUtility.httpOptions).pipe(
      map((data: AuthResponceModel) => {
        if (data) {
          localStorage.setItem('user_roles', data.user_roles);
          localStorage.setItem('user_id', data.user_id);
          localStorage.setItem('access_token', data.access_token);
          localStorage.setItem('user_name', data.user_name);
          if (this.UserInRole(Role.ADMIN)) {
            this.router.navigate(['admin']);
          } else if (this.UserInRole(Role.PLAYER) || this.UserInRole(Role.TESTER)) {
            this.router.navigate(['user']);
          }
        }
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('auth', this.toastr))
    );
  }

  logout() {
    return this.http.post(this.authEndpoint + 'SignOut/', JSON.stringify({}), ServiceUtility.getHttpOptionsWithAuth(this)).pipe(
      map(data => {
        localStorage.removeItem('user_roles');
        localStorage.removeItem('user_id');
        localStorage.removeItem('access_token');
        localStorage.removeItem('user_name');
        this.router.navigate(['signin']);
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('auth', this.toastr))
    );

  }

  signUp(regForm) {
    return this.http.post<{ access_token: string }>(this.authEndpoint + 'Register/',
      JSON.stringify(regForm), ServiceUtility.httpOptions)
      .pipe(map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('register', this.toastr, false)));
  }

  restorePassword(email: string) {
    return this.http.post(this.authEndpoint + 'ForgotPassword/' + email,
      JSON.stringify({}), ServiceUtility.httpOptions)
      .pipe(map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('restorePassword', this.toastr, false)));
  }

  reSendEmailConfirm(email: string) {
    return this.http.get(this.authEndpoint + 'ReSendEmailConfirm/' + email, ServiceUtility.httpOptions)
      .pipe(map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('reSendEmailConfirm', this.toastr, false)));
  }

  public getAccessToken(): string {
    return localStorage.getItem('access_token');
  }

  public IsLoggedIn(): boolean {
    return localStorage.getItem('access_token') !== null && localStorage.getItem('access_token') !== '';
  }

  public getUserRoles(): string {
    return localStorage.getItem('user_roles');
  }

  public UserInRole(role: string): boolean {
    const roles = localStorage.getItem('user_roles');
    if (roles.indexOf(role) !== -1) {
      return true;
    }
    return false;
  }

  public getUserId(): string {
    return localStorage.getItem('user_id');
  }

  public getUserName(): string {
    return localStorage.getItem('user_name');
  }
}
