import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../auth/auth.service';
import {ServiceUtility} from '../service-utility';
import {catchError, map} from 'rxjs/internal/operators';
import {TreasureModel} from '../../models/treasure.model';
import {CharacterModel} from '../../models/character.model';
import {Observable} from 'rxjs';
import {CharacterStarModel} from '../../models/character-star.model';
import {Config} from '../../app-config';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  constructor(private http: HttpClient, private toastr: ToastrService, private auth: AuthService,
              private config: Config) {
  }

  readonly endpoint = this.config.config.LOOTBOX_SERVICE_URL + '/api/v1/collection/';

  getCollection(): Observable<CharacterModel[]> {
    return this.http.get(this.endpoint + 'GetCollection', ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => ServiceUtility.extractRequestData(responce).map(character => new CharacterModel(
        character.id,
        character.name,
        character.image,
        character.startStar,
        character.code,
        character.treasures.map(treasure => new TreasureModel(treasure.id, treasure.name, treasure.code, [], '', 100))
        )),
        catchError(ServiceUtility.handleErrorWithToast<any>('getCollection', this.toastr, []))
      ));
  }

  getStars() {
    return this.http.get(this.endpoint + 'GetStars', ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => ServiceUtility.extractRequestData(responce).map(star => new CharacterStarModel(
        star.starCount,
        star.needFragments,
        star.borderColor
        )),
        catchError(ServiceUtility.handleErrorWithToast<any>('getCollection', this.toastr, []))
      ));
  }
}
