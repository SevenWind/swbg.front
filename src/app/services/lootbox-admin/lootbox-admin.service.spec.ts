import { TestBed } from '@angular/core/testing';

import { LootboxAdminService } from './lootbox-admin.service';

describe('LootboxAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LootboxAdminService = TestBed.get(LootboxAdminService);
    expect(service).toBeTruthy();
  });
});
