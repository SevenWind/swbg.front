import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/internal/operators';
import { TreasureModel } from '../../models/treasure.model';

import { ItemModel } from '../../models/item.model';
import { DropInfoModel } from '../../models/dropInfo.model';
import { ResourceModel } from '../../models/resource.model';
import { ServiceUtility } from '../service-utility';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth/auth.service';
import { Config } from '../../app-config';

@Injectable({
  providedIn: 'root'
})
export class AdminLootboxService {

  constructor(private http: HttpClient, private toastr: ToastrService,
    private auth: AuthService, private config: Config) {
  }

  readonly endpoint = this.config.config.LOOTBOX_SERVICE_URL + '/api/v1/admin/';

  getTreasures(): Observable<TreasureModel[]> {
    return this.http.get(this.endpoint + 'List', ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => ServiceUtility.extractRequestData(responce).map(treasure => new TreasureModel(
        treasure.id,
        treasure.name,
        treasure.code,
        treasure.resources.map(res =>
          new ResourceModel(
            new ItemModel(res.resource.item.id, res.resource.item.name, res.resource.item.rarity.code,
              res.resource.item.image), res.resource.value)),
        treasure.image,
        treasure.exp,
        treasure.isLocked
      ))),
      catchError(ServiceUtility.handleErrorWithToast<any>('getTreasures', this.toastr))
    );
  }

  lockTreasure(treasureid: string): Observable<boolean> {
    return this.http.post(this.endpoint + 'LockTreasure/' + treasureid, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('lockTreasure', this.toastr, false))
      );
  }

  unLockTreasure(treasureid: string): Observable<boolean> {
    return this.http.post(this.endpoint + 'UnLockTreasure/' + treasureid, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('unLockTreasure', this.toastr, false))
      );
  }

  saveTreasure(treasure: TreasureModel): Observable<boolean> {
    return this.http.post(this.endpoint + 'SaveTreasure/', JSON.stringify({
      id: treasure.id,
      name: treasure.name,
      code: treasure.code,
      image: treasure.image,
      exp: treasure.exp,
      resources: treasure.resources.map(res => {
        return {
          value: res.value,
          item_id: res.item.id,
          isDeleted: res.isDeleted
        };
      })
    }),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('saveTreasure', this.toastr, false))
      );
  }

  saveDropInfo(treasureid: string, dropInfo: DropInfoModel[]): Observable<boolean> {
    return this.http.post(this.endpoint + 'SaveDropInfo/' + treasureid, JSON.stringify(
      dropInfo.map(info => {
        return {
          item_id: info.item.id,
          chance: info.chance,
          count: info.count,
          exp: info.exp,
          frequency: info.frequency,
          isDeleted: info.isDeleted
        };
      })
    ),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('saveDropInfo', this.toastr, false))
      );
  }
}
