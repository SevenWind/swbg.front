import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, debounceTime, map, tap} from 'rxjs/internal/operators';
import {Observable} from 'rxjs';
import {UserShortInfoModel} from '../../models/userShortInfo.model';
import {ServiceUtility} from '../service-utility';
import {UserEditInfoModel} from '../../models/user-edit-info.model';
import {RatingItemModel} from '../../models/rating-item.model';
import {RatingModel} from '../../models/rating.model';
import {UserCashModel} from '../../models/userCash.model';
import {ShopModel} from '../../models/shop.model';
import {ShopItemModel} from '../../models/shop-item.model';
import {ItemCostModel} from '../../models/item-cost.model';
import {DropModel} from '../../models/drop.model';
import {CalendarRewardModel} from '../../models/calendar-reward.model';
import {ResourceModel} from '../../models/resource.model';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../auth/auth.service';
import {UserCollectionItemModel} from '../../models/user-collection-item.model';
import {UserXpInfoModel} from '../../models/user-xp-info.model';
import {Config} from '../../app-config';
import { ItemModel } from 'src/app/models/item.model';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient, private toastr: ToastrService,
              private auth: AuthService, private config: Config) {
  }

  readonly playerEndpoint = this.config.config.USER_SERVICE_URL + '/api/v1/player/';
  readonly calendarEndpoint = this.config.config.USER_SERVICE_URL + '/api/v1/calendar/';
  readonly collectionEndpoint = this.config.config.USER_SERVICE_URL + '/api/v1/collection/';
  readonly inventoryEndpoint = this.config.config.USER_SERVICE_URL + '/api/v1/inventory/';

  @Output() userCashChange: EventEmitter<UserCashModel> = new EventEmitter();
  @Output() userExpChange: EventEmitter<UserXpInfoModel> = new EventEmitter();

  getPlayerFullInfo(playerId): Observable<UserEditInfoModel> {
    return this.http.get(this.playerEndpoint + 'GetPlayerInfo/' + playerId, ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return new UserEditInfoModel(
          data.name,
          data.level,
          data.xp,
          data.nextXp,
          data.email,
          data.avatar_name,
          data.avatarImage,
          new Date(data.birthday),
          '',
          '');
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getPlayerFullInfo', this.toastr)));
  }

  getPlayerShortInfo(playerId): Observable<UserShortInfoModel> {
    return this.http.get(this.playerEndpoint + 'GetPlayerShortInfo/' + playerId, ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return new UserShortInfoModel(
          data.username,
          data.avatarImage,
          data.level.level,
          data.xp,
          data.cash.coins,
          data.cash.gold,
          data.level.needXpForNext
        );
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getPlayerShortInfo', this.toastr)));
  }

  updateCash(playerId): Observable<any> {
    return this.http.get(this.playerEndpoint + 'GetPlayerCash/' + playerId, ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        this.userCashChange.emit(new UserCashModel(data.coins, data.gold));
        return;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('updateCash', this.toastr)));
  }

  getRating(pageNumber, pageSize = 10): Observable<RatingModel> {
    return this.http.get(this.playerEndpoint + 'GetRating/pageNumber=' + pageNumber + '&pageSize=' + pageSize,
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return new RatingModel(
          data.rating.map(rate => new RatingItemModel(
            rate.position,
            rate.nick,
            rate.level,
            rate.currentXp,
            rate.xpToLvl)
          ),
          data.pageCount,
          data.pageNumber);
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getRating', this.toastr)));
  }

  donate(playerId: string, value: number): Observable<any> {
    return this.http.post(this.playerEndpoint + 'Donate/userId=' + playerId + '&value=' + value, '',
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => {
        this.updateCash(playerId).subscribe();
        return;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('donate', this.toastr))
    );
  }

  savePlayerProfile(playerId: string, userModel: UserEditInfoModel): Observable<any> {
    return this.http.put(this.playerEndpoint + 'SavePlayerInfo/' + playerId, JSON.stringify(userModel),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => _),
      catchError(ServiceUtility.handleErrorWithToast<any>('savePlayerProfile', this.toastr))
    );
  }

  buyItem(playerId, itemId, count, currency, cost): Observable<any> {
    return this.http.post(this.inventoryEndpoint + 'BuyItem/' + playerId, JSON.stringify({
      itemId: itemId,
      currency: currency,
      count: count,
      cost: cost
    }), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => {
        this.updateCash(playerId).subscribe();
        return true;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('buyItem', this.toastr, false))
    );
  }

  sellItem(playerId, itemId, count, currency, cost): Observable<any> {
    return this.http.post(this.inventoryEndpoint + 'SellItem/' + playerId, JSON.stringify({
      itemId: itemId,
      currency: currency,
      count: count,
      cost: cost
    }), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => {
        this.updateCash(playerId).subscribe();
        return;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('sellItem', this.toastr))
    );
  }

  getInventoryItems(playerId, searchString, pageNumber, pageSize = 10): Observable<ShopModel> {
    return this.http.get(
      this.inventoryEndpoint + 'GetInventoryItems/userId=' + playerId
      + '&searchString=' + (searchString.length ? searchString : '_')
      + '&pageNumber=' + pageNumber + '&pageSize=' + pageSize,
      ServiceUtility.getHttpOptionsWithAuth(this.auth)
    ).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return new ShopModel(
          data.items.map(shopItem => new ShopItemModel(
            shopItem.item.id,
            shopItem.item.name,
            shopItem.item.code,
            shopItem.item.rarity.code,
            shopItem.item.image,
            shopItem.count,
            new ItemCostModel(shopItem.buyGoldCost, shopItem.buyCoinCost),
            new ItemCostModel(shopItem.sellGoldCost, shopItem.sellCoinsCost),
            shopItem.isAvail
            )
          ),
          data.pageCount,
          data.pageNumber);
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getShopItems', this.toastr)));
  }

  addItemsToInventory(playerId: string, items: DropModel[], fromTreasure: boolean = false): Observable<any> {
    return this.http.post(this.inventoryEndpoint + 'AddItemsToInventory/userId=' + playerId + '&fromTreasure=' + fromTreasure,
      JSON.stringify(
        items.map(item => {
          return {
            itemId: item.item.id,
            currency: 'GOLD',
            count: item.count,
            cost: 0,
            xp: item.exp
          };
        })
      ), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => {
        return true;
      }),
      catchError(ServiceUtility.handleErrorWithToast<boolean>('addItemsToInventory', this.toastr, false))
    );
  }

  addExp(playerId: string, xp: number): Observable<any> {
    return this.http.post(this.inventoryEndpoint + 'AddXp/userId=' + playerId + '&xp=' + xp,
      JSON.stringify({}), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map((data: UserXpInfoModel) => {
        this.userExpChange.emit(new UserXpInfoModel(
          data.level,
          data.xp,
          data.xpToNextLvl
        ));
        return true;
      }),
      catchError(ServiceUtility.handleErrorWithToast<boolean>('addItemsToInventory', this.toastr, false))
    );
  }

  getCalendar(playerId: string): Observable<boolean[]> {
    return this.http.get(this.calendarEndpoint + 'GetUserCalendar/' + playerId, ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return data.days ? data.days.map(day => day.dayChecked) : [];
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getCalendar', this.toastr, [])));
  }

  getRewardStatuses(playerId: string): Observable<boolean[]> {
    return this.http.get(this.calendarEndpoint + 'GetUserCalendarRewardStatuses/' + playerId,
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return data.days ? data.days.map(day => day.rewardCollected) : [];
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getRewardStatuses', this.toastr, [])));
  }

  checkDay(playerId: string, dayNumber: number, cost: number, item: DropModel): Observable<any> {
    return this.http.post(this.calendarEndpoint + 'CheckDay/userId=' + playerId + '&dayNumber=' + dayNumber, JSON.stringify({
      itemId: item.item.id,
      currency: 'GOLD',
      count: item.count,
      cost: cost
    }), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => {
        this.updateCash(playerId).subscribe();
        return;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('checkDay', this.toastr))
    );
  }

  collectReward(playerId: string, reward: CalendarRewardModel): Observable<any> {
    return this.http.post(this.calendarEndpoint + 'CollectReward/' + playerId, JSON.stringify({
      id: reward.id,
      needCheckDaysCount: reward.needCheckDaysCount,
      name: reward.name,
      code: reward.code,
      items: reward.items.map(item => {
        return {
          itemId: item.item.id,
          count: item.count,
          cost: 0,
          currency: 'GOLD',
        };
      })
    }), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => {
        this.updateCash(playerId).subscribe();
        return;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('collectReward', this.toastr))
    );
  }

  getMailAddresses(search): Observable<any[]> {
    return this.http.get(this.playerEndpoint + 'GetMailAddresses/' + search, ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      debounceTime(500),
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        if (!data) {
          return [];
        }
        return data.map(address => {
          return {
            id: address.id,
            name: address.name
          };
        });
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getMailAddresses', this.toastr)));
  }

  canOpenTreasure(playerId: string, count: number, resources: ResourceModel[]): Observable<boolean> {
    return this.http.post(this.inventoryEndpoint + 'CanOpenTreasure/userId=' + playerId + '&count=' + count,
      JSON.stringify(resources.map(a => {
        return {
          itemId: a.item.id,
          count: a.value,
        };
      })), ServiceUtility.getHttpOptionsWithAuth(this.auth))
      .pipe(map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('canOpenTreasure', this.toastr, false)));
  }

  returnTreasureResources(playerId: string, count: number, resources: ResourceModel[]): Observable<boolean> {
    return this.http.post(this.inventoryEndpoint + 'ReturnTreasureResources/userId=' + playerId + '&count=' + count,
      JSON.stringify(resources.map(a => {
        return {
          itemId: a.item.id,
          count: a.value,
        };
      })), ServiceUtility.getHttpOptionsWithAuth(this.auth))
      .pipe(map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('returnTreasureResources', this.toastr, false)));
  }

  getUserCollection(playerId: string): Observable<UserCollectionItemModel[]> {
    return this.http.get(this.collectionEndpoint + 'GetUserCollection/' + playerId, ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return data.map(item => new UserCollectionItemModel(
          item.id,
          item.character_id,
          item.fragmentCount,
          item.starCount
        ));
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getUserCollection', this.toastr, [])));
  }

  deleteItem(itemId: string) {
    return this.http.post(this.inventoryEndpoint + 'DeleteItem/' + itemId, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('deleteItem', this.toastr, false)));
  }

  restoreItem(itemId: string) {
    return this.http.post(this.inventoryEndpoint + 'RestoreItem/' + itemId, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('restoreItem', this.toastr, false)));
  }

  saveItem(item: ItemModel): any {
    item.id = item.id === '' ? '00000000-0000-0000-0000-000000000000' : item.id;
    return this.http.post(this.inventoryEndpoint + 'SaveItem/', JSON.stringify({
      id: item.id,
      name: item.name,
      code: item.code,
      image: item.image,
      rarity_id: item.rarity
    }),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('saveItem', this.toastr, false)));
  }
}
