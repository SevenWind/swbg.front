import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MailModel } from '../../models/mail.model';
import { ServiceUtility } from '../service-utility';
import { catchError, map, tap } from 'rxjs/internal/operators';
import { DropModel } from '../../models/drop.model';
import { ItemModel } from '../../models/item.model';
import { MailBoxModel } from '../../models/mailbox.model';
import { MessageBoxType, Rarity } from '../../models/enums';
import { PlayerService } from '../player/player.service';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth/auth.service';
import { Config } from '../../app-config';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  constructor(private http: HttpClient, private playerService: PlayerService,
    private toastr: ToastrService, private auth: AuthService,
    private config: Config) {
  }

  readonly endpoint = this.config.config.MAIL_SERVICE_URL + '/api/v1/mail/';
  readonly adminEndpoint = this.config.config.MAIL_SERVICE_URL + '/api/v1/admin/';
  unreadedMessagesCount = 0;

  getMessages(userId: string, type: MessageBoxType, pageNumber: number,
    pageSize: number = 10, search: string = '_'): Observable<MailBoxModel> {
    return this.http.get(
      this.endpoint + 'GetMessages/userId=' + userId + '&search=' + search + '&type=' + type +
      '&pageNumber=' + pageNumber + '&pageSize=' + pageSize,
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(responce => {
          const data = ServiceUtility.extractRequestData(responce);
          if (type === MessageBoxType.Incoming) {
            this.unreadedMessagesCount = data.mails.filter(mail => !mail.isReaded).length;
          }
          return new MailBoxModel(data.mails.map(mail => {
            return new MailModel(
              mail.id,
              mail.fromId,
              mail.fromName,
              mail.toId,
              mail.toName,
              mail.theme,
              new Date(mail.date),
              mail.message,
              mail.isReaded,
              mail.attachmentCollected,
              mail.items.map(item => new DropModel(
                new ItemModel(item.item.id, item.item.name, item.item.rarity.code,
                  ServiceUtility.calcImageUrl(item.item.image, '../../../assets/')),
                item.count))
            );
          }),
            data.pageNumber,
            data.pageCount
          );
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('getIncomingMessages', this.toastr)));
  }

  sendMessage(mail: MailModel): Observable<any> {
    return this.http.post(this.endpoint + 'SendMail/', JSON.stringify({
      id: mail.id,
      fromId: mail.fromId,
      fromName: mail.fromName,
      toId: mail.toId,
      toName: mail.toName,
      theme: mail.theme,
      date: mail.date,
      message: mail.message,
      isReaded: mail.isReaded,
      attachmentCollected: mail.attachmentCollected,
      items: mail.items.map(item => {
        return {
          count: item.count,
          item_id: item.item.id
        }
      })
    }), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => _),
      catchError(ServiceUtility.handleErrorWithToast<any>('sendMessage', this.toastr))
    );
  }

  deleteMesage(userId: string, mailId: string): Observable<any> {
    return this.http.post(this.endpoint + 'DeleteMail/userId=' + userId + '&mailId=' + mailId, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => _),
        catchError(ServiceUtility.handleErrorWithToast<any>('deleteMesage', this.toastr))
      );
  }

  deleteMesages(userId: string, mails: string[]) {
    return this.http.post(this.endpoint + 'DeleteMails/' + userId, JSON.stringify(mails),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => _),
        catchError(ServiceUtility.handleErrorWithToast<any>('deleteMesages', this.toastr))
      );
  }

  checkMesage(userId: string, mailId: string): Observable<any> {
    return this.http.post(this.endpoint + 'CheckMail/userId=' + userId + '&mailId=' + mailId, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => _),
        catchError(ServiceUtility.handleErrorWithToast<any>('checkMesage', this.toastr))
      );
  }

  collectAttachment(userId: string, mail: MailModel): Observable<any> {
    return this.http.post(this.endpoint + 'CollectAttachment/' + userId, JSON.stringify({
      id: mail.id,
      fromId: mail.fromId,
      fromName: mail.fromName,
      toId: mail.toId,
      toName: mail.toName,
      theme: mail.theme,
      date: mail.date,
      message: mail.message,
      isReaded: mail.isReaded,
      attachmentCollected: mail.attachmentCollected,
      items: mail.items.map(item => {
        return {
          item_id: item.item.id,
          count: item.count
        };
      })
    }), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => {
        this.playerService.addItemsToInventory(userId, mail.items).subscribe(success => {
          if (!success) {
            this.http.post(this.endpoint + 'SetAttachmentNotCollected/userId=' + userId + '&mailId' + mail.id,
              JSON.stringify({}), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
                map(_ => _),
                catchError(ServiceUtility.handleErrorWithToast<any>('SetAttachmentNotCollected', this.toastr))
              ).subscribe();
          }
        });
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('collectAttachment', this.toastr))
    );
  }

  sendMessageWithAttach(mail: MailModel): Observable<boolean> {
    return this.http.post(this.endpoint + 'SendMailWithAttach/', JSON.stringify({
      fromId: mail.fromId,
      fromName: mail.fromName,
      toId: mail.toId,
      toName: mail.toName,
      theme: mail.theme,
      message: mail.message,
      items: mail.items.map(a => {
        return {
          item_id: a.item.id,
          count: a.count
        };
      })
    }), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(res => {
        return true;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('sendMessageWithAttach', this.toastr, false))
    );
  }

  sendMessages(mail: MailModel, toUserIds: any): Observable<boolean> {
    mail.id = '00000000-0000-0000-0000-000000000000';
    mail.toId = '00000000-0000-0000-0000-000000000000';
    return this.http.post(this.endpoint + 'SendMails/', JSON.stringify({
      fromId: mail.fromId,
      fromName: mail.fromName,
      theme: mail.theme,
      message: mail.message,
      userIds: toUserIds,
      items: mail.items.map(a => {
        return {
          item_id: a.item.id,
          count: a.count
        };
      })
    }), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(res => {
        return true;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('sendMessage', this.toastr, false))
    );
  }

  restoreItem(itemId: string): Observable<boolean> {
    return this.http.post(this.adminEndpoint + 'RestoreItem/' + itemId, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('restoreItem', this.toastr, false))
      );
  }

  deleteItem(itemId: string): Observable<boolean> {
    return this.http.post(this.adminEndpoint + 'DeleteItem/' + itemId, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('deleteItem', this.toastr, false))
      );
  }

  saveItem(item: ItemModel): any {
    item.id = item.id === '' ? '00000000-0000-0000-0000-000000000000' : item.id;
    return this.http.post(this.adminEndpoint + 'SaveItem/', JSON.stringify({
      id: item.id,
      name: item.name,
      code: item.code,
      image: item.image,
      rarity_id: item.rarity
    }),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('saveItem', this.toastr, false)));
  }
}
