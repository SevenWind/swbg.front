import {Injectable} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {ToastrService} from 'ngx-toastr';
import {PlayerService} from '../player/player.service';
import {HttpClient} from '@angular/common/http';
import {UserFilterModel} from '../../models/user-filter.model';
import {Observable} from 'rxjs';
import {UserListItemModel} from '../../models/user-list-item.model';
import {ServiceUtility} from '../service-utility';
import {catchError, map, tap} from 'rxjs/internal/operators';
import {Config} from '../../app-config';
import {UsersListModel} from '../../models/users-list.model';

@Injectable({
  providedIn: 'root'
})
export class AdminUsersService {

  constructor(private http: HttpClient, private playerService: PlayerService,
              private toastr: ToastrService, private auth: AuthService,
              private config: Config) {
  }

  readonly endpoint = this.config.config.USER_SERVICE_URL + '/api/v1/admin/';

  getUsers(filter: UserFilterModel, pageNum: number = 1, pageSize: number = 10): Observable<UsersListModel> {
    return this.http.post(this.endpoint + 'GetUsers/pageNum=' + pageNum + '&pageSize=' + pageSize,
      JSON.stringify(filter), ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return new UsersListModel(data.users.map(user => new UserListItemModel(
          user.id,
          user.name,
          user.lvl,
          user.email,
          user.regDate,
          user.lastOnlineDate,
          user.isLocked
          )),
          filter,
          data.pageNum,
          data.pageCount);
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getUserCollection', this.toastr, [])));
  }

  setUsersLockStatus(userIds: string[], lockStatus: boolean): Observable<boolean> {
    return this.http.post(this.endpoint + 'SetUsersLockStatus/status=' + lockStatus, JSON.stringify(userIds),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => {
        return true;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('setUsersLockStatus', this.toastr, false)));
  }

  addGoldToUsers(userIds: string[], gold: number) {
    return this.http.post(this.endpoint + 'AddGoldToUsers/gold=' + gold, JSON.stringify(userIds),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(_ => {
        return true;
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('addGoldToUsers', this.toastr, false)));
  }
}
