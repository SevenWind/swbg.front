import { TestBed } from '@angular/core/testing';

import { LootboxService } from './lootbox.service';

describe('LootboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LootboxService = TestBed.get(LootboxService);
    expect(service).toBeTruthy();
  });
});
