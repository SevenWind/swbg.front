import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/internal/operators';
import { TreasureModel } from '../../models/treasure.model';

import { ItemModel } from '../../models/item.model';
import { DropInfoModel } from '../../models/dropInfo.model';
import { DropModel } from '../../models/drop.model';
import { ResourceModel } from '../../models/resource.model';
import { ServiceUtility } from '../service-utility';
import { HistoryModel } from '../../models/history.model';
import { HistoryListModel } from '../../models/history-list.model';
import { PlayerService } from '../player/player.service';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth/auth.service';
import { Config } from '../../app-config';

@Injectable({
  providedIn: 'root'
})
export class LootboxService {

  constructor(private http: HttpClient, private playerService: PlayerService,
    private toastr: ToastrService, private auth: AuthService,
    private config: Config) {
  }

  readonly endpoint = this.config.config.LOOTBOX_SERVICE_URL + '/api/v1/lootbox/';
  readonly adminEndpoint = this.config.config.LOOTBOX_SERVICE_URL + '/api/v1/admin/';

  getTreasures(): Observable<TreasureModel[]> {
    return this.http.get(this.endpoint + 'list', ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => ServiceUtility.extractRequestData(responce).map(treasure => new TreasureModel(
        treasure.id,
        treasure.name,
        treasure.code,
        treasure.resources.map(res =>
          new ResourceModel(
            new ItemModel(res.resource.item.id, res.resource.item.name, res.resource.item.rarity.code,
              res.resource.item.image), res.resource.value)),
        treasure.image,
        treasure.exp
      ))),
      catchError(ServiceUtility.handleErrorWithToast<any>('getTreasures', this.toastr))
    );
  }

  getDropInfo(treasureid): Observable<any> {
    return this.http.get(this.endpoint + 'GetDropInfo/' + treasureid, ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
      map(responce => {
        const data = ServiceUtility.extractRequestData(responce);
        return {
          name: data.name,
          lootInfo: data.dropInfos.map(drop =>
            new DropInfoModel(
              new ItemModel(drop.item.id, drop.item.name, drop.item.rarity.code,
                drop.item.image.startsWith('data') ? drop.item.image : drop.item.image),
              drop.frequency.value, drop.chance, drop.count, drop.exp))
        };
      }),
      catchError(ServiceUtility.handleErrorWithToast<any>('getDropInfo', this.toastr))
    );
  }

  open(userId, treasureId, openCount, resources: ResourceModel[]): Observable<any> {
    return this.http.get(this.endpoint + 'open/userid=' + userId + '&treasureid=' + treasureId + '&count=' + openCount,
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(responce => {
          return ServiceUtility.extractRequestData(responce).map(drop => new DropModel(
            new ItemModel(drop.item.id, drop.item.name, drop.item.rarity.code, drop.item.image),
            drop.value,
            drop.exp
          ));
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('open', this.toastr, false))
      );
  }

  getHist(userId, pageNumber, pageSize = 10): Observable<HistoryListModel> {
    return this.http.get(this.endpoint + 'GetHist/userid=' + userId + '&pageNumber=' + pageNumber + '&pageSize=' + pageSize,
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(res => {
          const data = ServiceUtility.extractRequestData(res);
          const history = new HistoryListModel(
            data.history.map(hist =>
              new HistoryModel(
                hist.id,
                new Date(hist.date),
                hist.treasure_name,
                hist.item,
                hist.image,
                hist.count,
                hist.rarity
              )
            ),
            data.pageCount,
            data.pageNumber);
          return history;
        }
        ),
        catchError(ServiceUtility.handleErrorWithToast<any>('getHist', this.toastr))
      );
  }

  deleteItem(itemId: string) {
    return this.http.post(this.adminEndpoint + 'DeleteItem/' + itemId, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('deleteItem', this.toastr, false)));
  }

  restoreItem(itemId: string) {
    return this.http.post(this.adminEndpoint + 'RestoreItem/' + itemId, JSON.stringify({}),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('restoreItem', this.toastr, false)));
  }

  saveItem(item: ItemModel): any {
    item.id = item.id === '' ? '00000000-0000-0000-0000-000000000000' : item.id;
    return this.http.post(this.adminEndpoint + 'SaveItem/', JSON.stringify({
      id: item.id,
      name: item.name,
      code: item.code,
      image: item.image,
      rarity_id: item.rarity
    }),
      ServiceUtility.getHttpOptionsWithAuth(this.auth)).pipe(
        map(_ => {
          return true;
        }),
        catchError(ServiceUtility.handleErrorWithToast<any>('saveItem', this.toastr, false)));
  }
}

